//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file hadronic/Hadr03/include/DetectorConstruction.hh
/// \brief Definition of the DetectorConstruction class
//
//
// $Id$
//
// 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4Material.hh"

class G4AssemblyVolume;
class G4Material;
class G4LogicalVolume;
class G4Material;
class G4UniformMagField;
class DetectorMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
  
    DetectorConstruction();
   ~DetectorConstruction();

  public:
  
    virtual G4VPhysicalVolume* Construct();

    G4Material* 
    MaterialWithSingleIsotope(G4String, G4String, G4double, G4int, G4int);
         
    void SetSize     (G4double);              
    void SetMaterial (G4String);            
    void SetMagField (G4double);

    void UpdateGeometry();
     
  public:
  
     const
     G4VPhysicalVolume* GetWorld()      {return fPBox;};           
                    
     G4double           GetSize()       {return fBoxSize;};      
     G4Material*        GetMaterial()   {return fMaterial;};
     
     void               PrintParameters();
                       
  private:

  G4Material*  H2O;
  G4Material*  H2OMaterial;
  G4Material*  H3BO3;
  G4Material*  AcquaBorata;
  G4Material*  PbF;
  G4Material*  AlC;
  G4Material*  AW5083;
  
  G4LogicalVolume* logicWorld;  
  G4LogicalVolume* logicKilling;  
  G4VPhysicalVolume*    fPBox; 
  G4LogicalVolume*      fLBox; 
  G4LogicalVolume*      logicSpa;
  G4LogicalVolume*      logicAlSpa;
  G4LogicalVolume*      logicH2OSpa;
  G4LogicalVolume*      logicPorH2OSpa;
  G4LogicalVolume*      logicGH2OSpa;
  G4LogicalVolume*      logicG2H2OSpa;
  G4LogicalVolume*      logicG3H2OSpa;
  G4LogicalVolume*      logicG4H2OSpa;
  G4LogicalVolume*      logicG5H2OSpa;
  G4LogicalVolume*      logicG6H2OSpa;
  G4LogicalVolume*      logicG7H2OSpa;
  G4LogicalVolume*      logicG8H2OSpa;
  G4LogicalVolume*      logicG9H2OSpa;
  G4LogicalVolume*      logicPbSpa;
  G4LogicalVolume*      logicFrontAlSpa;
  G4LogicalVolume*      logicFront2AlSpa;
  G4LogicalVolume*      logicFront3H2OSpa;
  G4LogicalVolume*      logicFront4H2OSpa;
  G4LogicalVolume*      logicFront5AlSpa;
  G4LogicalVolume*      logicFront6AlSpa;
  G4LogicalVolume*      logicEnd1AlSpa;
  G4LogicalVolume*      logicEnd2H2OSpa;
  G4LogicalVolume*      logicEnd3AlSpa; 
  G4LogicalVolume*      logicEnd4H2OBSpa;
  // G4LogicalVolume*      logicGrid1;
 
  G4LogicalVolume*      logicGr1;
  G4LogicalVolume*      logicGr2;
  G4LogicalVolume*      logicGr3;
  G4LogicalVolume*      logicGr4;
  G4LogicalVolume*      logicGr5;
  //
  G4LogicalVolume*      logicGr6;
  G4LogicalVolume*      logicGr7;
  // G4LogicalVolume*      logicGr8;
  G4LogicalVolume*      logicGr9;
  // G4LogicalVolume*      logicGr10;
  G4LogicalVolume*      logicGr11;
  // G4LogicalVolume*      logicGr11bis;
  // G4LogicalVolume*      logicGr12;
  G4LogicalVolume*      logicGr13;
  // G4LogicalVolume*      logicGr14;
  G4LogicalVolume*      logicGr15;
  // G4LogicalVolume*      logicGr15bis;
  //
  G4LogicalVolume*      logicGr16;
  G4LogicalVolume*      logicGr17;
  // G4LogicalVolume*      logicGr18;
  G4LogicalVolume*      logicGr19;
  // G4LogicalVolume*      logicGr20;
  G4LogicalVolume*      logicGr21;
  // G4LogicalVolume*      logicGr22;
  G4LogicalVolume*      logicGr23;
  // G4LogicalVolume*      logicGr24;
  G4LogicalVolume*      logicGr25;
  //
  G4LogicalVolume*      logicGr26;
  G4LogicalVolume*      logicGr27;
  // G4LogicalVolume*      logicGr28;
  G4LogicalVolume*      logicGr29;
  // G4LogicalVolume*      logicGr30;
  G4LogicalVolume*      logicGr31;
  // G4LogicalVolume*      logicGr32;
  G4LogicalVolume*      logicGr33;
  // G4LogicalVolume*      logicGr34;
  G4LogicalVolume*      logicGr35;
  //
  G4LogicalVolume*      logicGr36;
  G4LogicalVolume*      logicGr37;
  //G4LogicalVolume*      logicGr38;
  G4LogicalVolume*      logicGr39;
  // G4LogicalVolume*      logicGr40;
  //  G4LogicalVolume*      logicGr41;
  G4LogicalVolume*      logicGr42;
  // G4LogicalVolume*      logicGr43;
  //
  G4LogicalVolume*      logicGr44;
  G4LogicalVolume*      logicGr45;
  // G4LogicalVolume*      logicGr46;
  G4LogicalVolume*      logicGr47;
  // G4LogicalVolume*      logicGr48;
  // G4LogicalVolume*      logicGr49;
  G4LogicalVolume*      logicGr50;
  // G4LogicalVolume*      logicGr51;
  //
 
  G4LogicalVolume*      logicAirEnd;
  G4LogicalVolume*      logicGrigliaEnd;
  G4LogicalVolume*      logicGriglia2End;
  G4LogicalVolume*      logicPixelGrid2;
  G4LogicalVolume*      logicDummy2Vuoto;
  G4LogicalVolume*      logicDummy2;
  G4LogicalVolume*      logicDum2UP;

 
  G4VPhysicalVolume*    physiWorld;
  G4VPhysicalVolume*    physiSpa;
  G4VPhysicalVolume*    physiAlSpa;
  G4VPhysicalVolume*    physiH2OSpa;
  G4VPhysicalVolume*    physiPbSpa;
  G4VPhysicalVolume*    physiKilling;
  
  G4double              fBoxSize;
  G4Material*           fMaterial;     
  G4UniformMagField*    fMagField;
  
  DetectorMessenger* fDetectorMessenger;

  // G4AssemblyVolume * GRID_0;
  
  G4VPhysicalVolume* Grid2;  // serve per la parametrizzazione
  private:
    
     void               DefineMaterials();
     G4VPhysicalVolume* ConstructVolumes();     

     const double temperature;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


#endif

