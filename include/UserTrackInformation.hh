#include "G4VUserTrackInformation.hh"
#include "globals.hh"

#ifndef UserTrackInformation_h
#define UserTrackInformation_h 1

class UserTrackInformation : public G4VUserTrackInformation
{
  public:
    UserTrackInformation();
    ~UserTrackInformation();

    inline void Print() const { G4cout << "nColls: " << nColls << G4endl; }

    inline void Increment() { nColls++; }

    inline G4int GetNColls() const { return nColls; }

  private:

    G4int nColls;
};

#endif
