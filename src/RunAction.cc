//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// -------------------------------------------------------------
//
//
// -------------------------------------------------------------
//      GEANT4
//
//
// -------------------------------------------------------------

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "RunAction.hh"
#include "HistoManager.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"
#include "G4NistManager.hh"
#include "G4Element.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include "Randomize.hh"
#include <iomanip>

// prova per accoppiare ABLA ad INCLXX
// vecchia versione//
//#include "G4HadronicInteraction.hh"
//#include "G4HadronicInteractionRegistry.hh"
//#include "G4INCLXXInterface.hh"
//#include "G4AblaInterface.hh"

#include "G4HadronicInteraction.hh"
#include "G4HadronicInteractionRegistry.hh"
#include "G4INCLXXInterface.hh"
#include "G4INCLXXInterfaceStore.hh"
#include "G4AblaInterface.hh"
#include <vector>


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

RunAction::RunAction(const G4String &xsecs, const G4String &outputFileName, const G4bool withColl) :
  crossSections(xsecs)
{
  fHistoManager = new HistoManager(outputFileName, withColl);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

RunAction::~RunAction()
{
 delete fHistoManager;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::BeginOfRunAction(const G4Run* aRun)
{
  G4int id = aRun->GetRunID();
  G4cout << "### Run " << id << " start" << G4endl;
 
 //histograms
  //
//  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
//  if ( analysisManager->IsActive() ) {
//    G4cout << "G4AnalysisManager opening file" << G4endl;
//    analysisManager->OpenFile();
//  }     

#ifdef G4VIS_USE
  G4UImanager* UI = G4UImanager::GetUIpointer();

  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();

  if(pVVisManager)
  {
    UI->ApplyCommand("/vis/scene/notifyHandlers");
  }
#endif

  // righe per accoppiare ABLA ad INCLXX
  // vecchia versione
  /*
// Get hold of a pointer to the INCL++ model interface
  G4HadronicInteraction *interaction = G4HadronicInteractionRegistry::Instance()
    ->FindModel("INCL++ v5.1.14");
  G4INCLXXInterface *theINCLInterface = static_cast<G4INCLXXInterface*>(interaction);

  if(theINCLInterface) {
    // Instantiate the ABLA model
    interaction = G4HadronicInteractionRegistry::Instance()->FindModel("ABLA");
    G4AblaInterface *theAblaInterface = static_cast<G4AblaInterface*>(interaction);
    if(!theAblaInterface)
      theAblaInterface = new G4AblaInterface;

    // Couple INCL++ to ABLA
    G4cout << "Coupling INCLXX to ABLA" << G4endl;
    theINCLInterface->SetDeExcitation(theAblaInterface);
  }
  */
  /*
  // TOGLIERE I COMMENTI SE SI VUOLE ABLA ACCOPPIATO AD INCLXX
  // Get hold of pointers to the INCL++ model interfaces
  std::vector<G4HadronicInteraction *> interactions = G4HadronicInteractionRegistry::Instance()->FindAllModels(G4INCLXXInterfaceStore::GetInstance()->getINCLXXVersionName());
  for(std::vector<G4HadronicInteraction *>::const_iterator iInter=interactions.begin(), e=interactions.end();
      iInter!=e; ++iInter) {
    G4INCLXXInterface *theINCLInterface = static_cast<G4INCLXXInterface*>(*iInter);
    if(theINCLInterface) {
      // Instantiate the ABLA model
      G4HadronicInteraction *interaction = G4HadronicInteractionRegistry::Instance()->FindModel("ABLA");
      G4AblaInterface *theAblaInterface = static_cast<G4AblaInterface*>(interaction);
      if(!theAblaInterface)
        theAblaInterface = new G4AblaInterface;
      // Couple INCL++ to ABLA
      G4cout << "Coupling INCLXX to ABLA" << G4endl;
      theINCLInterface->SetDeExcitation(theAblaInterface);
    }
  }
 // FINE TOGLIERE I COMMENTI SE SI VUOLE ABLA ACCOPPIATO AD INCLXX
 */
  G4INCL::Config &theConfig =G4INCLXXInterfaceStore::GetInstance()->GetINCLConfig();
  if(crossSections=="incl46") {
    G4cout << "Setting INCL46 cross sections for INCL++" << G4endl;
    theConfig.setCrossSectionsType(G4INCL::INCL46CrossSections);
  } else if(crossSections=="truncated3") {
    G4cout << "Setting truncated3 cross sections for INCL++" << G4endl;
    theConfig.setCrossSectionsType(G4INCL::TruncatedMultiPionsCrossSections);
    theConfig.setMaxNumberMultipions(3);
  } else if(crossSections=="truncated2") {
    G4cout << "Setting truncated3 cross sections for INCL++" << G4endl;
    theConfig.setCrossSectionsType(G4INCL::TruncatedMultiPionsCrossSections);
    theConfig.setMaxNumberMultipions(2);
  } else if(crossSections=="truncated1") {
    G4cout << "Setting truncated3 cross sections for INCL++" << G4endl;
    theConfig.setCrossSectionsType(G4INCL::TruncatedMultiPionsCrossSections);
    theConfig.setMaxNumberMultipions(1);
  } else if(crossSections=="multipion") {
    G4cout << "Setting multipion cross sections for INCL++" << G4endl;
    theConfig.setCrossSectionsType(G4INCL::MultiPionsCrossSections);
  } else {
    G4cerr << "Unrecognized value for the --cross-sections option" << G4endl;
    std::abort();
  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::EndOfRunAction(const G4Run*)
{
  G4cout << "RunAction: End of run actions are started" << G4endl;

#ifdef G4VIS_USE
  if (G4VVisManager::GetConcreteInstance())
    G4UImanager::GetUIpointer()->ApplyCommand("/vis/viewer/update");
#endif

 //save histograms      
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();  
  G4cout << "G4AnalysisManager is active: " << analysisManager->IsActive() << G4endl;
//  if ( analysisManager->IsActive() ) {
    G4cout << "G4AnalysisManager closing file" << G4endl;
    analysisManager->Write();
    analysisManager->CloseFile();
    delete analysisManager;
//  }


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
