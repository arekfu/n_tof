#include "ActionInitialization.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "EventAction.hh"
#include "SteppingAction.hh"
#include "StackingAction.hh"
#include "TrackingAction.hh"
#include "G4String.hh"
#include "DetectorConstruction.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ActionInitialization::ActionInitialization(const G4String &xsecs, const G4String &fname, const G4bool withColl) :
  G4VUserActionInitialization(),
  crossSections(xsecs),
  outputFileName(fname),
  withCollisionNtuple(withColl)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ActionInitialization::~ActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ActionInitialization::BuildForMaster() const
{
  SetUserAction(new RunAction(crossSections, outputFileName, withCollisionNtuple));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ActionInitialization::Build() const
{
  EventAction *eventAction = new EventAction;
  SetUserAction(eventAction);
  SetUserAction(new PrimaryGeneratorAction);
  SetUserAction(new RunAction(crossSections, outputFileName, withCollisionNtuple));
  SetUserAction(new SteppingAction(eventAction, withCollisionNtuple));
  SetUserAction(new StackingAction);
  SetUserAction(new TrackingAction);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
