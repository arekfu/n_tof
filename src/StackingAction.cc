//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: StackingAction.cc,v 1.5 2006/10/04 09:56:03 vnivanch Exp $
// GEANT4 tag $Name: geant4-09-00 $
//
/////////////////////////////////////////////////////////////////////////
//
// StackingAction
//
// Created: 31.04.2006 V.Ivanchenko
//
// Modified:
// 04.06.2006 Adoptation of hadr01 (V.Ivanchenko)
//
////////////////////////////////////////////////////////////////////////
// 

#include "StackingAction.hh"
#include "StackingMessenger.hh"

#include "G4Track.hh"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "G4RunManager.hh"
#include "HistoManager.hh"
#include <iostream>
#include <fstream>
#include <math.h>
#include "Randomize.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"

using namespace std;

/*
FILE *gam;
FILE *pi0;
FILE *neu;
FILE *pipiu;
FILE *pimeno;
*/

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

StackingAction::StackingAction() :
  modification(NORMAL)
{
  stackMessenger = new StackingMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

StackingAction::~StackingAction()
{
  delete stackMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ClassificationOfNewTrack
StackingAction::ClassifyNewTrack(const G4Track* aTrack)
{

  G4ParticleDefinition* par = aTrack->GetDefinition();

  if(modification==KILL_PI0 && par == G4PionZero::PionZero())
    return fKill;
  else if(modification==KILL_PIP_PIM && (par == G4PionPlus::PionPlus() || par==G4PionMinus::PionMinus()))
    return fKill;
  else if(modification==PI0_TO_PIP_PIM && par == G4PionZero::PionZero()) {
    G4DynamicParticle *newDP = new G4DynamicParticle(*aTrack->GetDynamicParticle());
    const G4double xi = G4UniformRand();
    if(xi<0.5)
      newDP->SetDefinition(G4PionPlus::PionPlus());
    else
      newDP->SetDefinition(G4PionMinus::PionMinus());
    G4Track *newTrack = new G4Track(newDP, aTrack->GetGlobalTime(), aTrack->GetPosition());
    stackManager->PushOneTrack(newTrack);
    return fKill;
  } else
    return fUrgent;
}

void StackingAction::setModification(const G4String &modStr)
{
  if(modStr=="kill_pi0")
    setModification(KILL_PI0);
  else if(modStr=="kill_pip_pim")
    setModification(KILL_PIP_PIM);
  else if(modStr=="pi0_to_pip_pim")
    setModification(PI0_TO_PIP_PIM);
  else if(modStr=="normal")
    setModification(NORMAL);
  else {
    G4cerr << "unrecognized modification mode" << G4endl;
    std::abort();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
