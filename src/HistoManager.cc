//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file hadronic/Hadr03/src/HistoManager.cc
/// \brief Implementation of the HistoManager class
//
// $Id: HistoManager.cc 72245 2013-07-12 08:51:51Z gcosmo $
// 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 

#include "HistoManager.hh"
#include "G4UnitsTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HistoManager::HistoManager(const G4String &fname, const G4bool withColl)
  : fFileName(fname)
{
  Book(withColl);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HistoManager::~HistoManager()
{
  //  delete G4AnalysisManager::Instance();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void HistoManager::Book(const G4bool withColl)
{
  // Create or get analysis manager
  // The choice of analysis technology is done via selection of a namespace
  // in HistoManager.hh
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->SetFileName(fFileName);
  analysisManager->OpenFile();
  analysisManager->SetVerboseLevel(1);
//  analysisManager->SetActivation(true);     //enable inactivation of histograms

  // Define histograms start values
  const G4int kMaxHisto = 13;
  const G4String id[] = {"0","1","2","3","4","5","6","7","8","9",
                         "10","11","12"};
  const G4String title[] = 
                { "dummy",                                          //0
                  "kinetic energy of scattered primary particle",   //1
                  "kinetic energy of recoil nuclei",                //2
                  "kinetic energy of gamma",                        //3
                  "kinetic energy of neutrons",                     //4
                  "kinetic energy of protons",                      //5
                  "kinetic energy of pi0",                    //6
                  "kinetic energy of pi+",                       //7
                  "kinetic energy of pi-",              //8
                  "nPi0",            //9
                  "nPiCharged",           //10
                  "nGamma",                         //11
                  "nNeutron"                    //12
                 };  

  // Default values (to be reset via /analysis/h1/set command)               
  G4int nbins = 100;
  G4double vmin = 0.;
  G4double vmax = 20000.;

  // Create all histograms as inactivated 
  // as we have not yet set nbins, vmin, vmax
  for (G4int k=0; k<kMaxHisto; k++) {
    G4int ih;
    if(k<9)
      ih = analysisManager->CreateH1(id[k], title[k], nbins, vmin, vmax);
    else
      ih = analysisManager->CreateH1(id[k], title[k], 100000, -0.5, 99999.5);
    analysisManager->SetH1Activation(ih, true);
  }

  // create the output ntuples
  // nflux1
  G4int nflux1ID = analysisManager->CreateNtuple("nflux1", "neutrons towards EAR1");
  G4cout << "creating ntuple " << nflux1ID << G4endl;
  analysisManager->CreateNtupleIColumn(nflux1ID, "eventID");
  analysisManager->CreateNtupleIColumn(nflux1ID, "parentID");
  analysisManager->CreateNtupleIColumn(nflux1ID, "trackID");
  analysisManager->CreateNtupleIColumn(nflux1ID, "nCollisions");
  analysisManager->CreateNtupleFColumn(nflux1ID, "x");
  analysisManager->CreateNtupleFColumn(nflux1ID, "y");
  analysisManager->CreateNtupleFColumn(nflux1ID, "z");
  analysisManager->CreateNtupleFColumn(nflux1ID, "tx");
  analysisManager->CreateNtupleFColumn(nflux1ID, "ty");
  analysisManager->CreateNtupleFColumn(nflux1ID, "tz");
  analysisManager->CreateNtupleFColumn(nflux1ID, "E");
  analysisManager->CreateNtupleFColumn(nflux1ID, "t");
  analysisManager->FinishNtuple();
  // gflux1
  G4int gflux1ID = analysisManager->CreateNtuple("gflux1", "gammas towards EAR1");
  G4cout << "creating ntuple " << gflux1ID << G4endl;
  analysisManager->CreateNtupleIColumn(gflux1ID, "eventID");
  analysisManager->CreateNtupleIColumn(gflux1ID, "parentID");
  analysisManager->CreateNtupleIColumn(gflux1ID, "trackID");
  analysisManager->CreateNtupleIColumn(gflux1ID, "nCollisions");
  analysisManager->CreateNtupleFColumn(gflux1ID, "x");
  analysisManager->CreateNtupleFColumn(gflux1ID, "y");
  analysisManager->CreateNtupleFColumn(gflux1ID, "z");
  analysisManager->CreateNtupleFColumn(gflux1ID, "tx");
  analysisManager->CreateNtupleFColumn(gflux1ID, "ty");
  analysisManager->CreateNtupleFColumn(gflux1ID, "tz");
  analysisManager->CreateNtupleFColumn(gflux1ID, "E");
  analysisManager->CreateNtupleFColumn(gflux1ID, "t");
  analysisManager->FinishNtuple();
  // nflux2
  G4int nflux2ID = analysisManager->CreateNtuple("nflux2", "neutrons towards EAR2");
  G4cout << "creating ntuple " << nflux2ID << G4endl;
  analysisManager->CreateNtupleIColumn(nflux2ID, "eventID");
  analysisManager->CreateNtupleIColumn(nflux2ID, "parentID");
  analysisManager->CreateNtupleIColumn(nflux2ID, "trackID");
  analysisManager->CreateNtupleIColumn(nflux2ID, "nCollisions");
  analysisManager->CreateNtupleFColumn(nflux2ID, "x");
  analysisManager->CreateNtupleFColumn(nflux2ID, "y");
  analysisManager->CreateNtupleFColumn(nflux2ID, "z");
  analysisManager->CreateNtupleFColumn(nflux2ID, "tx");
  analysisManager->CreateNtupleFColumn(nflux2ID, "ty");
  analysisManager->CreateNtupleFColumn(nflux2ID, "tz");
  analysisManager->CreateNtupleFColumn(nflux2ID, "E");
  analysisManager->CreateNtupleFColumn(nflux2ID, "t");
  analysisManager->FinishNtuple();
  // gflux2
  G4int gflux2ID = analysisManager->CreateNtuple("gflux2", "gammas towards EAR2");
  G4cout << "creating ntuple " << gflux2ID << G4endl;
  analysisManager->CreateNtupleIColumn(gflux2ID, "eventID");
  analysisManager->CreateNtupleIColumn(gflux2ID, "parentID");
  analysisManager->CreateNtupleIColumn(gflux2ID, "trackID");
  analysisManager->CreateNtupleIColumn(gflux2ID, "nCollisions");
  analysisManager->CreateNtupleFColumn(gflux2ID, "x");
  analysisManager->CreateNtupleFColumn(gflux2ID, "y");
  analysisManager->CreateNtupleFColumn(gflux2ID, "z");
  analysisManager->CreateNtupleFColumn(gflux2ID, "tx");
  analysisManager->CreateNtupleFColumn(gflux2ID, "ty");
  analysisManager->CreateNtupleFColumn(gflux2ID, "tz");
  analysisManager->CreateNtupleFColumn(gflux2ID, "E");
  analysisManager->CreateNtupleFColumn(gflux2ID, "t");
  analysisManager->FinishNtuple();
  // pi0
  G4int pi0ID = analysisManager->CreateNtuple("pi0", "pi0");
  G4cout << "creating ntuple " << pi0ID << G4endl;
  analysisManager->CreateNtupleIColumn(pi0ID, "eventID");
  analysisManager->CreateNtupleIColumn(pi0ID, "parentID");
  analysisManager->CreateNtupleIColumn(pi0ID, "trackID");
  analysisManager->CreateNtupleFColumn(pi0ID, "x");
  analysisManager->CreateNtupleFColumn(pi0ID, "y");
  analysisManager->CreateNtupleFColumn(pi0ID, "z");
  analysisManager->CreateNtupleFColumn(pi0ID, "E");
  analysisManager->FinishNtuple();

  // collision sites
  if(withColl) {
    G4int collsID = analysisManager->CreateNtuple("colls", "collisions");
    G4cout << "creating ntuple " << collsID << G4endl;
    analysisManager->CreateNtupleIColumn(collsID, "eventID");
    analysisManager->CreateNtupleIColumn(collsID, "parentID");
    analysisManager->CreateNtupleIColumn(collsID, "trackID");
    analysisManager->CreateNtupleIColumn(collsID, "pdg");
    analysisManager->CreateNtupleIColumn(collsID, "nSecondaries");
    analysisManager->CreateNtupleIColumn(collsID, "nCollisions");
    analysisManager->CreateNtupleIColumn(collsID, "subtype");
    analysisManager->CreateNtupleIColumn(collsID, "targetA");
    analysisManager->CreateNtupleIColumn(collsID, "targetZ");
    analysisManager->CreateNtupleFColumn(collsID, "x");
    analysisManager->CreateNtupleFColumn(collsID, "y");
    analysisManager->CreateNtupleFColumn(collsID, "z");
    analysisManager->CreateNtupleFColumn(collsID, "E");
    analysisManager->CreateNtupleFColumn(collsID, "t");
    analysisManager->FinishNtuple();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
