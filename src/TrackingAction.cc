#include "TrackingAction.hh"
#include "UserTrackInformation.hh"
#include "G4Track.hh"
#include "G4TrackingManager.hh"

void TrackingAction::PreUserTrackingAction(const G4Track *track) {
  if(track->GetParentID()==0) {
    UserTrackInformation *info = new UserTrackInformation;
    fpTrackingManager->SetUserTrackInformation(info);
  }
}

void TrackingAction::PostUserTrackingAction(const G4Track *track) {
  G4TrackVector* secondaries = fpTrackingManager->GimmeSecondaries();
  if(secondaries)
  {
    UserTrackInformation* info = (UserTrackInformation*)(track->GetUserInformation());
    size_t nSeco = secondaries->size();
    if(nSeco>0)
    {
      for(size_t i=0; i < nSeco; i++)
      {
        UserTrackInformation* infoNew = new UserTrackInformation(*info);
        (*secondaries)[i]->SetUserInformation(infoNew);
      }
    }
  }
}
