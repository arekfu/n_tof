//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file hadronic/Hadr03/src/DetectorConstruction.cc
/// \brief Implementation of the DetectorConstruction class
//

//
// $Id$
//
// 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4UniformMagField.hh"
#include "G4VisAttributes.hh"
#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4Colour.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

// per la parametrizzazione

#include "ChamberParameterisation.hh"
#include "G4PVParameterised.hh"
//per l'assembly
#include  "G4AssemblyVolume.hh"
#include "G4PVReplica.hh"
// per calcolare il valore assoluto e fare eventualmente delle print
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <math.h>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
:fPBox(0), fLBox(0), fMaterial(0), fMagField(0), temperature(300.)
{
  fBoxSize = 60*cm;
  DefineMaterials();
  SetMaterial("Uranium238");  
  fDetectorMessenger = new DetectorMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ delete fDetectorMessenger;}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
  return ConstructVolumes();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::DefineMaterials()
{
 // define a Material from isotopes
 //
 MaterialWithSingleIsotope("Boron10",      "B10",    2.46*g/cm3, 5,  10);
 MaterialWithSingleIsotope("Boron11",      "B11",    2.46*g/cm3, 5,  11);
 MaterialWithSingleIsotope("Oxygen16",     "O16",    1.43*g/cm3, 8,  16);
 MaterialWithSingleIsotope("Cacium40",     "Ca40",   1.55*g/cm3, 20, 40);
 MaterialWithSingleIsotope("Zirconium90",  "Zr90",   6.51*g/cm3, 40, 90);    
 MaterialWithSingleIsotope("Molybdenum98", "Mo98",  10.28*g/cm3, 42, 98);
 MaterialWithSingleIsotope("Molybdenum100","Mo100", 10.28*g/cm3, 42, 100); 
 MaterialWithSingleIsotope("Lead208",      "Pb208", 11.34*g/cm3, 82, 208); 
 MaterialWithSingleIsotope("Uranium235",   "U235",  19.05*g/cm3, 92, 235);  
 MaterialWithSingleIsotope("Uranium238",   "U238",  19.05*g/cm3, 92, 238);     
 MaterialWithSingleIsotope("Uranium234",   "U234",  19.05*g/cm3, 92, 234);     
    
 // or use G4-NIST materials data base
 //
 G4NistManager* man = G4NistManager::Instance();
 man->FindOrBuildMaterial("G4_B");

 G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* DetectorConstruction::MaterialWithSingleIsotope( G4String name,
                           G4String symbol, G4double density, G4int Z, G4int A)
{
 // define a material from an isotope
 //
 G4int ncomponents;
 G4double abundance, massfraction;

 G4Isotope* isotope = new G4Isotope(symbol, Z, A);
 
 G4Element* element  = new G4Element(name, symbol, ncomponents=1);
 element->AddIsotope(isotope, abundance= 100.*perCent);
 
 G4Material* material = new G4Material(name, density, ncomponents=1, kStateUndefined, temperature);
 material->AddElement(element, massfraction=100.*perCent);

 return material;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::ConstructVolumes()
{
  // Cleanup old geometry
  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore::GetInstance()->Clean();
  G4LogicalVolumeStore::GetInstance()->Clean();
  G4SolidStore::GetInstance()->Clean();


  G4String name, symbol;             // a=mass of a mole;
  G4double a;             
  G4int zint;
  G4int nint;
  G4double A;  // atomic mass
  G4double Z;  // atomic number
  G4double N;  // nb of number of nucleons
  G4double d;  // density
  G4int ncomponents;
  G4double abundance,fractionmass;
  // G4double temperature, pressure;

// define an Element from isotopes, by relative abundance 
  G4Isotope* Li6 = new G4Isotope(name="Li6", zint=3, nint=6, A=6.941*g/mole);
  G4Element* elLi6  = new G4Element(name="elLi6", symbol="Li", ncomponents=1);
  elLi6->AddIsotope(Li6, abundance= 100.*perCent);
  G4Isotope* Sr90 = new G4Isotope(name="Sr90", zint=38, nint=90, A=87.620*g/mole);
  G4Element* elSr90  = new G4Element(name="elSr90", symbol="Sr", ncomponents=1);
  elSr90->AddIsotope(Sr90, abundance= 100.*perCent);
  G4Isotope* Th232 = new G4Isotope(name="Th232", zint=90, nint=232, A=232.038*g/mole);
  G4Element* elTh232  = new G4Element(name="elTh232", symbol="Th", ncomponents=1);
  elTh232->AddIsotope(Th232, abundance= 100.*perCent);
  G4Isotope* U238 = new G4Isotope(name="U238", zint=92, nint=238, A=238.029*g/mole);
  G4Element* elU238  = new G4Element(name="elU238", symbol="U", ncomponents=1);
  elU238->AddIsotope(U238, abundance= 100.*perCent);
  // Boro arricchito al 95%
  G4Isotope* Boro10 = new G4Isotope(name="B10", zint=5, nint=10, A = 10.811*g/mole);
  G4Isotope* Boro11 = new G4Isotope(name="B11", zint=5, nint=11, A = 10.811*g/mole);
  G4Element* elBoro10  = new G4Element(name="elBoro10", symbol="B", ncomponents=2); 
  elBoro10->AddIsotope(Boro10, abundance= 95.*perCent);
  elBoro10->AddIsotope(Boro11, abundance= 5.*perCent);
 
  
  G4Element* elH  = new G4Element("TS_H_of_Water" ,"H" , 1., 1.0079*g/mole);
  A = 6.941*g/mole;
  G4Element* elLi = new G4Element("Li","Li",Z = 3.,A);
  A = 10.811*g/mole;
  G4Element* elB  = new G4Element(name="Boron",symbol="B" , Z= 5., A);
  A = 16.00*g/mole;
  G4Element* elO = new G4Element("Oxygen","O",Z = 8.,A);  
  A = 22.99*g/mole;
  G4Element* elNa = new G4Element("Na","Na",Z = 11.,A);  
  A = 24.3050*g/mole;
  G4Element* elMg  = new G4Element(name="Mg",symbol="Mg" , Z= 12., A); 
  A = 26.981538*g/mole;
  G4Element* elAl  = new G4Element(name="Aluminium",symbol="Al" , Z= 13., A);
  A = 28.0855*g/mole;
  G4Element* elSi  = new G4Element(name="Silicio",symbol="Si" , Z= 14., A);
  A = 30.97*g/mole;
  G4Element* elP  = new G4Element(name="P",symbol="P" , Z= 15., A);
  A = 32.07*g/mole;
  G4Element* elS  = new G4Element(name="S",symbol="S" , Z= 16., A);
  A = 39.1*g/mole;
  G4Element* elK  = new G4Element(name="K",symbol="K" , Z= 19., A);
  A = 40.8*g/mole;
  G4Element* elCa  = new G4Element(name="Ca",symbol="Ca" , Z= 20., A);
  A = 44.956*g/mole;
  G4Element* elSc  = new G4Element(name="Sc",symbol="Sc" , Z= 21., A);
  A = 47.867*g/mole;
  G4Element* elTi  = new G4Element(name="Ti",symbol="Ti" , Z= 22., A);
  A = 50.942*g/mole;
  G4Element* elV  = new G4Element(name="V",symbol="V" , Z= 23., A);
  A = 51.9961*g/mole;
  G4Element* elCr  = new G4Element(name="Cr",symbol="Cr" , Z= 24., A);
  A = 54.938049*g/mole;
  G4Element* elMn  = new G4Element(name="Mn",symbol="Mn" , Z= 25., A); 
  A = 55.84*g/mole;
  G4Element* elFe  = new G4Element(name="Ferro",symbol="Fe" , Z= 26., A);
  A = 58.69*g/mole;
  G4Element* elNi  = new G4Element(name="Ni",symbol="Ni" , Z= 28., A);
  A = 63.546*g/mole;
  G4Element* elCu  = new G4Element(name="Rame",symbol="Cu" , Z= 29., A);
  A = 65.409*g/mole;
  G4Element* elZn  = new G4Element(name="Zn",symbol="Zn" , Z= 30., A);
  A = 69.723*g/mole;
  G4Element* elGa  = new G4Element(name="Ga",symbol="Ga" , Z= 31., A);
  A = 72.610*g/mole;
  G4Element* elGe  = new G4Element(name="Ge",symbol="Ge" , Z= 32., A);
  A = 74.922*g/mole;
  G4Element* elAs  = new G4Element(name="As",symbol="As" , Z= 33., A);
  A = 78.960*g/mole;
  G4Element* elSe  = new G4Element(name="Se",symbol="Se" , Z= 34., A);
  A = 79.904*g/mole;
  G4Element* elBr  = new G4Element(name="Br",symbol="Br" , Z= 35., A);
  A = 85.496*g/mole;
  G4Element* elRb  = new G4Element(name="Rb",symbol="Rb" , Z= 37., A);
  A = 87.620*g/mole;
  G4Element* elSr  = new G4Element(name="Sr",symbol="Sr" , Z= 38., A);
  A = 88.906*g/mole;
  G4Element* elY  = new G4Element(name="Y",symbol="Y" , Z= 39., A);
  A = 91.224*g/mole;
  G4Element* elZr  = new G4Element(name="Zr",symbol="Zr" , Z= 40., A);
  A = 92.906*g/mole;
  G4Element* elNb  = new G4Element(name="Nb",symbol="Nb" , Z= 41., A);
  A = 95.940*g/mole;
  G4Element* elMo  = new G4Element(name="Mo",symbol="Mo" , Z= 42., A);
  A = 101.070*g/mole;
  G4Element* elRu  = new G4Element(name="Ru",symbol="Ru" , Z= 44., A);
  A = 102.906*g/mole;
  G4Element* elRh  = new G4Element(name="Rh",symbol="Rh" , Z= 45., A);
  A = 106.420*g/mole;
  G4Element* elPd  = new G4Element(name="Pd",symbol="Pd" , Z= 46., A);
  A = 107.9*g/mole;
  G4Element* elAg  = new G4Element(name="Ag",symbol="Ag" , Z= 47., A);
  A = 112.411*g/mole;
  G4Element* elCd  = new G4Element(name="Cd",symbol="Cd" , Z= 48., A);
  A = 114.820*g/mole;
  G4Element* elIn  = new G4Element(name="In",symbol="In" , Z= 49., A);
  A = 118.7*g/mole;
  G4Element* elSn  = new G4Element(name="Sn",symbol="Sn" , Z= 50., A);
  A = 121.760*g/mole;
  G4Element* elSb  = new G4Element(name="Sb",symbol="Sb" , Z= 51., A);//antimonio
  A = 127.60*g/mole;
  G4Element* elTe  = new G4Element(name="Te",symbol="Te" , Z= 52., A);
  A = 126.904*g/mole;
  G4Element* elI  = new G4Element(name="I",symbol="I" , Z= 53., A);
  A = 132.905*g/mole;
  G4Element* elCs  = new G4Element(name="Cs",symbol="Cs" , Z= 55., A);//cesio
  A = 137.327*g/mole;
  G4Element* elBa  = new G4Element(name="Ba",symbol="Ba" , Z= 56., A);
  A = 138.906*g/mole;
  G4Element* elLa  = new G4Element(name="La",symbol="La" , Z= 57., A);
  A = 140.115*g/mole;
  G4Element* elCe  = new G4Element(name="Ce",symbol="Ce" , Z= 58., A);
  A = 140.908*g/mole;
  G4Element* elPr  = new G4Element(name="Pr",symbol="Pr" , Z= 59., A);//praseodino
  A = 144.240*g/mole;
  G4Element* elNd  = new G4Element(name="Nd",symbol="Nd" , Z= 60., A);
  A = 178.490*g/mole;
  G4Element* elHf  = new G4Element(name="Hf",symbol="Hf" , Z= 72., A);
  A = 180.9*g/mole;
  G4Element* elTa  = new G4Element(name="Ta",symbol="Ta" , Z= 73., A);
  A = 183.8*g/mole;
  G4Element* elW  = new G4Element(name="W",symbol="W" , Z= 74., A);//tungsteno
  A = 186.207*g/mole;
  G4Element* elRe  = new G4Element(name="Re",symbol="Re" , Z= 75., A);
  A = 190.200*g/mole;
  G4Element* elOs  = new G4Element(name="Os",symbol="Os" , Z= 76., A);
  A = 192.220*g/mole;
  G4Element* elIr  = new G4Element(name="Ir",symbol="Ir" , Z= 77., A);
  A = 197.0*g/mole;
  G4Element* elAu  = new G4Element(name="Au",symbol="Au" , Z= 79., A);
  A = 204.3833*g/mole;
  G4Element* elTl  = new G4Element(name="Tl",symbol="Tl" , Z= 81., A);
  A = 207.2*g/mole;
  G4Element* elPb  = new G4Element(name="Pb",symbol="Pb" , Z= 82., A);
  A = 208.98038*g/mole;
  G4Element* elBi  = new G4Element(name="Bi",symbol="Bi" , Z= 83., A);

 // Pb FLUKA

  d = 11.35*g/cm3;
  G4Material* PbF = new G4Material(name="PbF",d,ncomponents=38, kStateUndefined, temperature);
  PbF->AddElement(elPb,fractionmass=99.99740*perCent);
  PbF->AddElement(elLi6,fractionmass=2.65E-4*perCent);
  PbF->AddElement(elO,fractionmass=4.871E-8*perCent);
  PbF->AddElement(elB,fractionmass=2.745E-7*perCent);
  PbF->AddElement(elNa,fractionmass=5.9E-7*perCent);
  PbF->AddElement(elMg,fractionmass=3.15E-7*perCent);
  PbF->AddElement(elAl,fractionmass=1.9E-6*perCent);
  PbF->AddElement(elSi,fractionmass=1.278E-6*perCent);
  PbF->AddElement(elP,fractionmass=7.18E-8*perCent);
  PbF->AddElement(elS,fractionmass=3.984E-6*perCent);
  PbF->AddElement(elK,fractionmass=1.896E-6*perCent);
  PbF->AddElement(elCa,fractionmass=1.599E-6*perCent);
  PbF->AddElement(elTi,fractionmass=3.453E-8*perCent);
  PbF->AddElement(elCr,fractionmass=1.54E-6*perCent);
  PbF->AddElement(elFe,fractionmass=7.035E-7*perCent);
  PbF->AddElement(elNi,fractionmass=3.332E-6*perCent);
  PbF->AddElement(elCu,fractionmass=9.5E-6*perCent);
  PbF->AddElement(elGa,fractionmass=3.039E-7*perCent);
  PbF->AddElement(elGe,fractionmass=2.599E-7*perCent);
  PbF->AddElement(elBr,fractionmass=1.157E-6*perCent);
  PbF->AddElement(elSr90,fractionmass=1.273E-8*perCent);
  PbF->AddElement(elCd,fractionmass=8.8E-6*perCent);
  PbF->AddElement(elZr,fractionmass=4.069E-8*perCent);
  PbF->AddElement(elAg,fractionmass=3.78E-4*perCent);
  PbF->AddElement(elIn,fractionmass=1.539E-7*perCent);
  PbF->AddElement(elSn,fractionmass=1.35E-5*perCent);
  PbF->AddElement(elSb,fractionmass=3.634E-7*perCent);
  PbF->AddElement(elI,fractionmass=3.138E-7*perCent);
  PbF->AddElement(elCs,fractionmass=5.464E-7*perCent);
  PbF->AddElement(elBa,fractionmass=4.769E-8*perCent);
  PbF->AddElement(elCe,fractionmass=1.12E-8*perCent);
  PbF->AddElement(elPr,fractionmass=1.549E-8*perCent);
  PbF->AddElement(elTa,fractionmass=5.0E-6*perCent);
  PbF->AddElement(elW,fractionmass=5.584E-7*perCent);
  PbF->AddElement(elAu,fractionmass=7.9E-8*perCent);
  PbF->AddElement(elBi,fractionmass=1.9E-3*perCent);
  PbF->AddElement(elTh232,fractionmass=5.21E-8*perCent);
  PbF->AddElement(elU238,fractionmass=2.084E-8*perCent);


  // Al di Cristian

  d = 2.699*g/cm3;
  G4Material* AlC = new G4Material(name="AlC",d,ncomponents=9, kStateUndefined, temperature);
  AlC->AddElement(elAl,fractionmass=95.35*perCent);
  AlC->AddElement(elSi,fractionmass=1.3*perCent);
  AlC->AddElement(elFe,fractionmass=0.5*perCent);
  AlC->AddElement(elCu,fractionmass=0.1*perCent);
  AlC->AddElement(elMn,fractionmass=1.0*perCent);
  AlC->AddElement(elMg,fractionmass=1.2*perCent);
  AlC->AddElement(elZn,fractionmass=0.2*perCent);
  AlC->AddElement(elTi,fractionmass=0.1*perCent);
  AlC->AddElement(elCr,fractionmass=0.25*perCent);
 
  // AW5083 di Fluka (che assomiglia ad Al di Cristian)

  d = 2.66*g/cm3;
  G4Material* AW5083 = new G4Material(name="AW5083",d,ncomponents=9, kStateUndefined, temperature);
  AW5083->AddElement(elAl,fractionmass=93.35*perCent);
  AW5083->AddElement(elSi,fractionmass=0.4*perCent);
  AW5083->AddElement(elFe,fractionmass=0.4*perCent);
  AW5083->AddElement(elCu,fractionmass=0.1*perCent);
  AW5083->AddElement(elMn,fractionmass=0.7*perCent);
  AW5083->AddElement(elMg,fractionmass=4.5*perCent);
  AW5083->AddElement(elZn,fractionmass=0.25*perCent);
  AW5083->AddElement(elTi,fractionmass=0.15*perCent);
  AW5083->AddElement(elCr,fractionmass=0.15*perCent);


  H2OMaterial = G4NistManager::Instance()->FindOrBuildMaterial("G4_WATER"); 

  // H20

  d = 1.000*g/cm3;
  G4Material* H2O = new G4Material(name="H2O",d,ncomponents=2, kStateUndefined, temperature);
  H2O->AddElement(elH, 2);
  H2O->AddElement(elO, 1);

   // H3BO3

  d = 1.44*g/cm3;
  G4Material* H3BO3 = new G4Material(name="H3BO3",d,ncomponents=3, kStateUndefined, temperature);
  H3BO3->AddElement(elH, 3);
  H3BO3->AddElement(elBoro10, 1);
  H3BO3->AddElement(elO, 3);

 //Nuova acqua borata 05/03/2015 dopo Mail di Calviani
  d = 1.0*g/cm3;
  G4Material* AcquaBorata = new G4Material(name="AcquaBorata",d,ncomponents=2, kStateUndefined, temperature);
  AcquaBorata->AddMaterial(H2O, fractionmass=95.8*perCent);//era 95.8% o 98.72%
  AcquaBorata->AddMaterial(H3BO3, fractionmass=4.2*perCent);//era 4.2% o 1.28%
 

  // Acqua Borata al 10% (era 1.28%)

  //1.28
  //   d = 1.005632*g/cm3; //quando c'era 1.28% (1.0X0.9872 + 1.44X0.0128)
  // G4Material* AcquaBorata = new G4Material(name="AcquaBorata",d,ncomponents=2, kStateUndefined, temperature);
  //  AcquaBorata->AddMaterial(H2OMaterial, fractionmass=98.72*perCent);
  // //AcquaBorata->AddElement(elB, fractionmass=1.28*perCent);
  //AcquaBorata->AddMaterial(Boron10, fractionmass=1.28*perCent);


  // d = 1.006072*g/cm3;
  // G4Material* AcquaBorata = new G4Material(name="AcquaBorata",d,ncomponents=2, kStateUndefined, temperature);
  // AcquaBorata->AddMaterial(H2OMaterial, fractionmass=98.62*perCent);
  // AcquaBorata->AddElement(elB, fractionmass=1.38*perCent);

  //Nuova acqua borata 02/03/2015
  // d = 1.018688*g/cm3; //quando c'era 1.28% (1.0X0.9872 + 2.46X0.0128)
  // G4Material* AcquaBorata = new G4Material(name="AcquaBorata",d,ncomponents=2, kStateUndefined, temperature);
  // AcquaBorata->AddMaterial(H2OMaterial, fractionmass=98.72*perCent);
  //AcquaBorata->AddElement(elBoro10, fractionmass=1.28*perCent);
 



 // Sizes

  G4double worldR = 10000.0*m;
  G4double worldZ = 10000.0*m;


  //-------------------------------------------------------------------------
  // World
  //
  G4Tubs* solidW = new G4Tubs("World",0.,worldR,worldZ,0.,twopi);
  G4Material *galactic =
    G4NistManager::Instance()->BuildMaterialWithNewDensity("HotGalactic", "G4_Galactic", 1e-30*g/cm3, temperature);
  logicWorld = new G4LogicalVolume( solidW, galactic, "World");
  //  logicWorld = new G4LogicalVolume( solidW, G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR"),"World");
  G4VPhysicalVolume* physiWorld = new G4PVPlacement(0,G4ThreeVector(),
                                       logicWorld,"physiWorld",0,false,0);
  // fine World---------------------------------------------------------------

  // Cilindro di AW5083 che contiene la targhetta di spallazione e la sua acqua di raffr

  G4double AlSpaZ = (400.0/2)*mm;
  G4double AlSpaR = (350.0)*mm;//era 335
  G4double AlPxSpa = 0.0*mm;
  G4double AlPySpa = 0.0*mm;
  G4double AlPzSpa = 0.0*mm;
  G4ThreeVector positionAlSpa = G4ThreeVector(AlPxSpa,AlPySpa,AlPzSpa);
  G4Tubs* solidAlSpa = new G4Tubs("solidAlSpa",0.,AlSpaR,AlSpaZ,0.,twopi);
  logicAlSpa = new G4LogicalVolume(solidAlSpa,AW5083,"logicAlSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionAlSpa,  // at (x,y,z)
		    logicAlSpa,     // its logical volume				  
		    "physiAlSpa",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro di AW5083---------------------------------------------------------------

 // Cilindro di H2O (contenuto in AW5083) che contiene la targhetta di spallazione 

  G4double H2OSpaZ = (400.0/2)*mm;
  G4double H2OSpaR = (314.0)*mm;
  G4double H2OPxSpa = 0.0*mm;
  G4double H2OPySpa = 0.0*mm;
  G4double H2OPzSpa = 0.0*mm;
  G4ThreeVector positionH2OSpa = G4ThreeVector(H2OPxSpa,H2OPySpa,H2OPzSpa);
  G4Tubs* solidH2OSpa = new G4Tubs("solidH2OSpa",0.,H2OSpaR,H2OSpaZ,0.,twopi);
  logicH2OSpa = new G4LogicalVolume(solidH2OSpa, H2O ,"logicH2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionH2OSpa,  // at (x,y,z)
		    logicH2OSpa,     // its logical volume				  
		    "physiH2OSpa",        // its name
		    logicAlSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro di H2O----------------------------------------------------------

 // Porzione di Cilindro di H2O verso EAR2 che sovrasta il rivestimento d'acqua  

  G4double PorH2OSpaZ = (400.0/2)*mm;
  G4double PorH2OSpaRest = (329.0)*mm;
  G4double PorH2OSpaRint = (314.0)*mm;
  G4double PorH2OPxSpa = 0.0*mm;
  G4double PorH2OPySpa = 0.0*mm;
  G4double PorH2OPzSpa = 0.0*mm;
  G4ThreeVector positionPorH2OSpa = G4ThreeVector(PorH2OPxSpa,PorH2OPySpa,PorH2OPzSpa);
  G4Tubs* solidPorH2OSpa = new G4Tubs("solidPorH2OSpa",PorH2OSpaRint,PorH2OSpaRest,PorH2OSpaZ,(pi/3.0),(pi/3.0));
  logicPorH2OSpa = new G4LogicalVolume(solidPorH2OSpa, H2O ,"logicPorH2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionPorH2OSpa,  // at (x,y,z)
		    logicPorH2OSpa,     // its logical volume				  
		    "physiPorH2OSpa",        // its name
		    logicAlSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine Porzione cilindro di H2O----------------------------------------------------------


  // INIZIO GRIGLIAMENTO DEL CILINDRO H2OSpa

  // dati comuni
  G4double GH2OSpaZ = (4.0/2)*mm;
  G4double GH2OSpaR = (314.0)*mm;
  G4double GH2OSpaRin = (304.0)*mm;
  G4double GH2OPxSpa = 0.0*mm;
  G4double GH2OPySpa = 0.0*mm;
  G4double GH2OPzSpa = 0.0*mm;
  // fine dati comuni

  G4double G1H2OPzSpa = 0.0*mm;
  G4double G1H2OPySpa = 0.0*mm;
  G4double G1H2OPxSpa = 0.0*mm;
  G4ThreeVector positionGH2OSpa = G4ThreeVector(G1H2OPxSpa,G1H2OPySpa,G1H2OPzSpa);
  G4Tubs* solidGH2OSpa = new G4Tubs("solidGH2OSpa",GH2OSpaRin,GH2OSpaR,GH2OSpaZ,0.,twopi);
  logicGH2OSpa = new G4LogicalVolume(solidGH2OSpa, AW5083 ,"logicGH2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionGH2OSpa,  // at (x,y,z)
		    logicGH2OSpa,     // its logical volume				  
		    "physiGH2OSpa",        // its name
		    logicH2OSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  G4double G2H2OPzSpa = (45.0+ 2*GH2OSpaZ)*mm;
  G4double G2H2OPySpa = 0.0*mm;
  G4double G2H2OPxSpa = 0.0*mm;
  G4ThreeVector positionG2H2OSpa = G4ThreeVector(G2H2OPxSpa,G2H2OPySpa,G2H2OPzSpa);
  G4Tubs* solidG2H2OSpa = new G4Tubs("solidG2H2OSpa",GH2OSpaRin,GH2OSpaR,GH2OSpaZ,0.,twopi);
  logicG2H2OSpa = new G4LogicalVolume(solidG2H2OSpa, AW5083 ,"logicG2H2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionG2H2OSpa,  // at (x,y,z)
		    logicG2H2OSpa,     // its logical volume				  
		    "physiG2H2OSpa",        // its name
		    logicH2OSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 

  G4double G3H2OPzSpa = (2*45.0 + 4*GH2OSpaZ)*mm;
  G4double G3H2OPySpa = 0.0*mm;
  G4double G3H2OPxSpa = 0.0*mm;
  G4ThreeVector positionG3H2OSpa = G4ThreeVector(G3H2OPxSpa,G3H2OPySpa,G3H2OPzSpa);
  G4Tubs* solidG3H2OSpa = new G4Tubs("solidG3H2OSpa",GH2OSpaRin,GH2OSpaR,GH2OSpaZ,0.,twopi);
  logicG3H2OSpa = new G4LogicalVolume(solidG3H2OSpa, AW5083 ,"logicG3H2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionG3H2OSpa,  // at (x,y,z)
		    logicG3H2OSpa,     // its logical volume				  
		    "physiG3H2OSpa",        // its name
		    logicH2OSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  G4double G4H2OPzSpa = (3*45.0 + 6*GH2OSpaZ)*mm;
  G4double G4H2OPySpa = 0.0*mm;
  G4double G4H2OPxSpa = 0.0*mm;
  G4ThreeVector positionG4H2OSpa = G4ThreeVector(G4H2OPxSpa,G4H2OPySpa,G4H2OPzSpa);
  G4Tubs* solidG4H2OSpa = new G4Tubs("solidG4H2OSpa",GH2OSpaRin,GH2OSpaR,GH2OSpaZ,0.,twopi);
  logicG4H2OSpa = new G4LogicalVolume(solidG4H2OSpa, AW5083 ,"logicG4H2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionG4H2OSpa,  // at (x,y,z)
		    logicG4H2OSpa,     // its logical volume				  
		    "physiG4H2OSpa",        // its name
		    logicH2OSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 

  G4double G5H2OPzSpa = -(45.0+ 2*GH2OSpaZ)*mm;
  G4double G5H2OPySpa = 0.0*mm;
  G4double G5H2OPxSpa = 0.0*mm;
  G4ThreeVector positionG5H2OSpa = G4ThreeVector(G5H2OPxSpa,G5H2OPySpa,G5H2OPzSpa);
  G4Tubs* solidG5H2OSpa = new G4Tubs("solidG5H2OSpa",GH2OSpaRin,GH2OSpaR,GH2OSpaZ,0.,twopi);
  logicG5H2OSpa = new G4LogicalVolume(solidG5H2OSpa, AW5083 ,"logicG5H2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionG5H2OSpa,  // at (x,y,z)
		    logicG5H2OSpa,     // its logical volume				  
		    "physiG5H2OSpa",        // its name
		    logicH2OSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 

  G4double G6H2OPzSpa =-(2*45.0 + 4*GH2OSpaZ)*mm;
  G4double G6H2OPySpa = 0.0*mm;
  G4double G6H2OPxSpa = 0.0*mm;
  G4ThreeVector positionG6H2OSpa = G4ThreeVector(G6H2OPxSpa,G6H2OPySpa,G6H2OPzSpa);
  G4Tubs* solidG6H2OSpa = new G4Tubs("solidG6H2OSpa",GH2OSpaRin,GH2OSpaR,GH2OSpaZ,0.,twopi);
  logicG6H2OSpa = new G4LogicalVolume(solidG6H2OSpa, AW5083 ,"logicG6H2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionG6H2OSpa,  // at (x,y,z)
		    logicG6H2OSpa,     // its logical volume				  
		    "physiG6H2OSpa",        // its name
		    logicH2OSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 

  G4double G7H2OPzSpa =-(3*45.0 + 6*GH2OSpaZ)*mm;
  G4double G7H2OPySpa = 0.0*mm;
  G4double G7H2OPxSpa = 0.0*mm;
  G4ThreeVector positionG7H2OSpa = G4ThreeVector(G7H2OPxSpa,G7H2OPySpa,G7H2OPzSpa);
  G4Tubs* solidG7H2OSpa = new G4Tubs("solidG7H2OSpa",GH2OSpaRin,GH2OSpaR,GH2OSpaZ,0.,twopi);
  logicG7H2OSpa = new G4LogicalVolume(solidG7H2OSpa, AW5083 ,"logicG7H2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionG7H2OSpa,  // at (x,y,z)
		    logicG7H2OSpa,     // its logical volume				  
		    "physiG7H2OSpa",        // its name
		    logicH2OSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 

  G4double G8H2OPzSpa =-(4*45.0 + 7*GH2OSpaZ + (6.0/2.0))*mm;
  G4double G8H2OPySpa = 0.0*mm;
  G4double G8H2OPxSpa = 0.0*mm;
  G4ThreeVector positionG8H2OSpa = G4ThreeVector(G8H2OPxSpa,G8H2OPySpa,G8H2OPzSpa);
  G4Tubs* solidG8H2OSpa = new G4Tubs("solidG8H2OSpa",GH2OSpaRin,GH2OSpaR,3.0*mm,0.,twopi);
  logicG8H2OSpa = new G4LogicalVolume(solidG8H2OSpa, AW5083 ,"logicG8H2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionG8H2OSpa,  // at (x,y,z)
		    logicG8H2OSpa,     // its logical volume				  
		    "physiG8H2OSpa",        // its name
		    logicH2OSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 

  G4double G9H2OPzSpa =(4*45.0 + 7*GH2OSpaZ + (6.0/2.0))*mm;
  G4double G9H2OPySpa = 0.0*mm;
  G4double G9H2OPxSpa = 0.0*mm;
  G4ThreeVector positionG9H2OSpa = G4ThreeVector(G9H2OPxSpa,G9H2OPySpa,G9H2OPzSpa);
  G4Tubs* solidG9H2OSpa = new G4Tubs("solidG9H2OSpa",GH2OSpaRin,GH2OSpaR,3.0*mm,0.,twopi);
  logicG9H2OSpa = new G4LogicalVolume(solidG9H2OSpa, AW5083 ,"logicG9H2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionG9H2OSpa,  // at (x,y,z)
		    logicG9H2OSpa,     // its logical volume				  
		    "physiG9H2OSpa",        // its name
		    logicH2OSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // FINE GRIGLIAMENTO DEL CILINDRO H2OSpa

// Cilindro di Pb (contenuto nel cilindro di H2O) che contiene la targhetta di spallazione 

  G4double PbSpaZ = (400.0/2)*mm;
  G4double PbSpaR = (300.0)*mm;
  G4double PbPxSpa = 0.0*mm;
  G4double PbPySpa = 0.0*mm;
  G4double PbPzSpa = 0.0*mm;
  G4ThreeVector positionPbSpa = G4ThreeVector(PbPxSpa,PbPySpa,PbPzSpa);
  G4Tubs* solidPbSpa = new G4Tubs("solidPbSpa",0.,PbSpaR,PbSpaZ,0.,twopi);
  logicPbSpa = new G4LogicalVolume(solidPbSpa,PbF,"logicPbSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionPbSpa,  // at (x,y,z)
		    logicPbSpa,     // its logical volume				  
		    "physiPbSpa",        // its name
		    logicH2OSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro di Pb---------------------------------------------------------



  // targhetta di spallazione----------------------------------------------

  G4double SpaZ = (380.0/2)*mm;
  G4double SpaR = (290.0)*mm;
  G4double PxSpa = 0.0*mm;
  G4double PySpa = 0.0*mm;
  G4double PzSpa = 0.0*mm;
  G4ThreeVector positionSpa = G4ThreeVector(PxSpa,PySpa,PzSpa);
  G4Tubs* solidSpa = new G4Tubs("solidSpa",0.,SpaR,SpaZ,0.,twopi);
  logicSpa = new G4LogicalVolume(solidSpa,PbF,"logicSpa",0,0,0);//rimettere Pb NIST
  new G4PVPlacement(0,               // no rotation
		    positionSpa,  // at (x,y,z)
		    logicSpa,     // its logical volume				  
		    "physiSpa",        // its name
		    logicPbSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 

  // fine targhetta di spallazione-----------------------------------------------

  // Cilindro forato di AW5083, prima parte del Front 

  G4double FrontAlSpaZ = (10.0/2)*mm;
  G4double FrontAlSpaRint = (300.0)*mm;
  G4double FrontAlPzSpa = (-AlSpaZ - FrontAlSpaZ)*mm;
  G4ThreeVector positionFrontAlSpa = G4ThreeVector(AlPxSpa,AlPySpa,FrontAlPzSpa);
  G4Tubs* solidFrontAlSpa = new G4Tubs("solidFrontAlSpa",FrontAlSpaRint,AlSpaR,FrontAlSpaZ,0.,twopi);
  logicFrontAlSpa = new G4LogicalVolume(solidFrontAlSpa,AW5083,"logicFrontAlSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionFrontAlSpa,  // at (x,y,z)
		    logicFrontAlSpa,     // its logical volume				  
		    "physiFrontAlSpa",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro forato di AW5083---------------------------------------------------------------

// Cilindro H2O, seconda parte del Front 

  G4double Front3H2OSpaZ = (3.0/2.0)*mm;
  G4double Front3H2OSpaR = (200.0)*mm;
  G4double Front3H2OSpaPz = (-AlSpaZ - Front3H2OSpaZ)*mm; 
  G4ThreeVector positionFront3H2OSpa = G4ThreeVector(AlPxSpa,AlPySpa,Front3H2OSpaPz);
  G4Tubs* solidFront3H2OSpa = new G4Tubs("solidFront3H2OSpa",0.0,Front3H2OSpaR,Front3H2OSpaZ,0.,twopi);
  logicFront3H2OSpa = new G4LogicalVolume(solidFront3H2OSpa,H2O,"logicFront3H2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionFront3H2OSpa,  // at (x,y,z)
		    logicFront3H2OSpa,     // its logical volume				  
		    "physiFront3H2OSpa",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro di H2O (seconda parte)---------------------------------------------------------------


  // Cilindro forato di AW5083, terza parte del Front 

  G4double Front2AlSpaZ = (3.0/2.0)*mm;
  G4double Front2AlSpaRint = (200.0)*mm;
  G4double Front2AlPzSpa = (Front3H2OSpaPz)*mm;
  G4ThreeVector positionFront2AlSpa = G4ThreeVector(AlPxSpa,AlPySpa,Front2AlPzSpa);
  G4Tubs* solidFront2AlSpa = new G4Tubs("solidFront2AlSpa",Front2AlSpaRint,FrontAlSpaRint,Front2AlSpaZ,0.,twopi);
  logicFront2AlSpa = new G4LogicalVolume(solidFront2AlSpa,AW5083,"logicFront2AlSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionFront2AlSpa,  // at (x,y,z)
		    logicFront2AlSpa,     // its logical volume				  
		    "physiFront2AlSpa",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro forato di AW5083 (terza parte)---------------------------------------------------------------



  // Cilindro H2O, quarta parte del Front 

  G4double Front4H2OSpaZ = (7.0/2.0)*mm;
  G4double Front4H2OPzSpa = (Front3H2OSpaPz - Front3H2OSpaZ - Front4H2OSpaZ)*mm; 
  G4ThreeVector positionFront4H2OSpa = G4ThreeVector(AlPxSpa,AlPySpa,Front4H2OPzSpa);
  G4Tubs* solidFront4H2OSpa = new G4Tubs("solidFront4H2OSpa",0.0,FrontAlSpaRint,Front4H2OSpaZ,0.,twopi);
  logicFront4H2OSpa = new G4LogicalVolume(solidFront4H2OSpa,H2O,"logicFront4H2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionFront4H2OSpa,  // at (x,y,z)
		    logicFront4H2OSpa,     // its logical volume				  
		    "physiFront4H2OSpa",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro di H2O (quarta parte)---------------------------------------------------------------

  // Cilindro AW5083, quinta parte del Front 

  G4double Front5AlSpaZ = (3.0/2.0)*mm;
  G4double Front5AlPzSpa = (Front4H2OPzSpa -Front4H2OSpaZ -Front5AlSpaZ)*mm; 
  G4ThreeVector positionFront5AlSpa = G4ThreeVector(AlPxSpa,AlPySpa,Front5AlPzSpa);
  G4Tubs* solidFront5AlSpa = new G4Tubs("solidFront5AlSpa",0.0,Front3H2OSpaR,Front5AlSpaZ,0.,twopi);
  logicFront5AlSpa = new G4LogicalVolume(solidFront5AlSpa,AW5083,"logicFront5AlSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionFront5AlSpa,  // at (x,y,z)
		    logicFront5AlSpa,     // its logical volume				  
		    "physiFront5AlSpa",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro di AW5083 (quinta parte)---------------------------------------------------------------

  // Cilindro AW5083, sesta parte del Front 

  G4double Front6AlSpaZ = (30.0/2.0)*mm;
  G4double Front6AlSpaRest = 350.0*mm;
  G4double Front6AlSpaRint = 200.0*mm;
  G4double Front6AlPzSpa = (Front4H2OPzSpa -Front4H2OSpaZ -Front6AlSpaZ)*mm; 
  G4ThreeVector positionFront6AlSpa = G4ThreeVector(AlPxSpa,AlPySpa,Front6AlPzSpa);
  G4Tubs* solidFront6AlSpa = new G4Tubs("solidFront6AlSpa",Front6AlSpaRint,Front6AlSpaRest,Front6AlSpaZ,0.,twopi);
  logicFront6AlSpa = new G4LogicalVolume(solidFront6AlSpa,AW5083,"logicFront6AlSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionFront6AlSpa,  // at (x,y,z)
		    logicFront6AlSpa,     // its logical volume				  
		    "physiFront6AlSpa",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro di AW5083 ( sesta parte)---------------------------------------------------------------


  // Cilindro AW5083, prima parte del end 

  G4double End1AlSpaZ = (56.0/2.0)*mm;//era 54.0/2.0
  G4double End1AlSpaRest = 350.0*mm;
  G4double End1AlPzSpa = (AlSpaZ + End1AlSpaZ)*mm; 
  G4ThreeVector positionEnd1AlSpa = G4ThreeVector(AlPxSpa,AlPySpa, End1AlPzSpa);
  G4Tubs* solidEnd1AlSpa = new G4Tubs("solidEnd1AlSpa",0.0,End1AlSpaRest,End1AlSpaZ,0.,twopi);
  logicEnd1AlSpa = new G4LogicalVolume(solidEnd1AlSpa,AW5083,"logicEnd1AlSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionEnd1AlSpa,  // at (x,y,z)
		    logicEnd1AlSpa,     // its logical volume				  
		    "physiEnd1AlSpa",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro di AW5083 del end (prima parte)---------------------------------------------------------------

  // Cilindro H20, seconda parte del end 

  G4double End2H2OSpaZ = (10.0/2.0)*mm;
  G4double End2H2OSpaRest = 300.0*mm;
  G4double End2H2OPzSpa = (-End1AlSpaZ + End2H2OSpaZ)*mm; 
  G4ThreeVector positionEnd2H2OSpa = G4ThreeVector(0.0,0.0, End2H2OPzSpa);
  G4Tubs* solidEnd2H2OSpa = new G4Tubs("solidEnd2H2OSpa",0.0,End2H2OSpaRest,End2H2OSpaZ,0.,twopi);
  logicEnd2H2OSpa = new G4LogicalVolume(solidEnd2H2OSpa,H2O,"logicEnd2H2OSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionEnd2H2OSpa,  // at (x,y,z)
		    logicEnd2H2OSpa,     // its logical volume				  
		    "physiEnd2H2OSpa",        // its name
		    logicEnd1AlSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro di H20 del end (seconda parte)---------------------------------------------------------------

 // Cilindro bucato di AW5083, terza parte del end 

  G4double End3AlSpaZ = (4.0/2.0)*mm;
  G4double End3AlSpaRest = 300.0*mm;
  G4double End3AlSpaRint = 200.0*mm;
  G4double End3AlPzSpa = (-End2H2OSpaZ + End3AlSpaZ)*mm; 
  G4ThreeVector positionEnd3AlSpa = G4ThreeVector(0.0,0.0, End3AlPzSpa);
  G4Tubs* solidEnd3AlSpa = new G4Tubs("solidEnd3AlSpa",End3AlSpaRint,End3AlSpaRest,End3AlSpaZ,0.,twopi);
  logicEnd3AlSpa = new G4LogicalVolume(solidEnd3AlSpa,AW5083,"logicEnd3AlSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionEnd3AlSpa,  // at (x,y,z)
		    logicEnd3AlSpa,     // its logical volume				  
		    "physiEnd3AlSpa",        // its name
		    logicEnd2H2OSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro bucato di AW5083 del end (terza parte)---------------------------------------------------------------


// Cilindro H20Borata, quarta parte del end 

  G4double End4H2OBSpaZ = (40.0/2.0)*mm;
  G4double End4H2OBSpaRest = 300.0*mm;
  G4double End4H2OBPzSpa = (-End1AlSpaZ + 2.0*End2H2OSpaZ + 3.0*mm + End4H2OBSpaZ)*mm; 
  G4ThreeVector positionEnd4H2OBSpa = G4ThreeVector(0.0,0.0, End4H2OBPzSpa);
  G4Tubs* solidEnd4H2OBSpa = new G4Tubs("solidEnd4H2OBSpa",0.0,End4H2OBSpaRest,End4H2OBSpaZ,0.,twopi);
  logicEnd4H2OBSpa = new G4LogicalVolume(solidEnd4H2OBSpa,AcquaBorata,"logicEnd4H2OBSpa",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionEnd4H2OBSpa,  // at (x,y,z)
		    logicEnd4H2OBSpa,     // its logical volume				  
		    "physiEnd4H2OBSpa",        // its name
		    logicEnd1AlSpa,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine cilindro di H20Borata quarta parte del end ---------------------------------------------------------------

  /*
 // ASSEMBLY PROVA acqua borata
  
  G4double PixelGrid1_x = (100.0/2.0)*mm ;  
  G4double PixelGrid1_y = (100.0/2.0)*mm ;  
  G4double PixelGrid1_z = (End4H2OBSpaZ)*mm ;  

  
  G4Box* PixelGrid1
    = new G4Box("PixelGrid1", PixelGrid1_x ,PixelGrid1_y ,PixelGrid1_z); 
  
  
  logicGrid1 = new G4LogicalVolume(PixelGrid1,galactic,"logicGrid1");


  G4ThreeVector VAt;
  G4RotationMatrix MAt;
  G4AssemblyVolume* GRID_0 = new G4AssemblyVolume();
  VAt.set( 0.0, 0.0, 0.0);


  // prova assembly dentro acqua borata
  G4double shiftGrid = 5.5*mm;
  G4int imax = (int)(End4H2OBSpaRest/(2.0*PixelGrid1_x));
  G4int jmax = (int)(End4H2OBSpaRest/(2.0*PixelGrid1_y));
  
      for(G4int i=-imax;i<=imax ;i++)
	{
	  for(G4int j=-jmax;j<=jmax ;j++)
	    {
	      G4double D_x0 =(0.0 + (i*2.0*PixelGrid1_x) + i*shiftGrid)*mm;
	      // G4double D_x0 =(0.0 + (i*2.0*PixelGrid1_z))*mm;  
	      G4double D_y0 =(0.0 + (j*2.0*PixelGrid1_y) + j*shiftGrid)*mm; 
	      // G4double D_y0 =(0.0 + (j*2.0*PixelGrid1_z))*mm; 
	      G4double Dx0P = fabs(D_x0 + PixelGrid1_x); 
	      G4double Dx0M = fabs(D_x0 - PixelGrid1_x);
	      G4double Dy0P = fabs(D_y0 + PixelGrid1_y); 
	      G4double Dy0M = fabs(D_y0 - PixelGrid1_y); 
	      G4double D_z0 = 0.0; 
	      G4double Dx0 = 0.0;
	      G4double Dy0 = 0.0;
	      if(Dx0P >= Dx0M) Dx0 = Dx0P;
	      if(Dx0P < Dx0M) Dx0 = Dx0M;
	      if(Dy0P >= Dy0M) Dy0 = Dy0P;
	      if(Dy0P < Dy0M) Dy0 = Dy0M;
	      if(((Dx0*Dx0)+(Dy0*Dy0))<(End4H2OBSpaRest*End4H2OBSpaRest))
		{
		  VAt.set( D_x0, D_y0, D_z0);
		  G4Transform3D gtranA_0(MAt, VAt); 
		  GRID_0 -> AddPlacedVolume(logicGrid1, gtranA_0);
		} // se i centri dei pixel sono dentro al cerchio dell'acqua borata     
	    }//serie j      
	}// serie i
 
  
  // fine  prova assembly dentro acqua borata

  VAt.set(0.0*mm, 0.0*mm, 0.0*mm);
  G4RotationMatrix AMbis(0.0*rad, 0.0*rad, 0.0*rad);
  G4Transform3D gAA(AMbis, VAt);
  GRID_0 -> MakeImprint(logicEnd4H2OBSpa, gAA);

 
  // FINE PROVA ASSEMBLY acqua borata

  */


  
 // INIZIO SLAB LUNGO ASSE Y a diverse posizioni di X

  G4double PzComune = 0.0*mm;// era 12.5*mm
  G4double GrzComune = (40.0/2.0)*mm;// era (15.0/2.0)*mm;

  // prima slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr1x = (5.5/2.0)*mm;//era solo 5.5
  G4double Gr1y = 298.5*mm;
  G4double Gr1z = GrzComune;
  G4double PxGr1 = 0.0*mm;
  G4double PyGr1 = 0.0*mm;
  G4double PzGr1 = PzComune;
  G4ThreeVector positionGr1 = G4ThreeVector(PxGr1,PyGr1,PzGr1);
  G4Box* solidGr1 = new G4Box("solidGr1",Gr1x,Gr1y,Gr1z);
  logicGr1 = new G4LogicalVolume(solidGr1,AW5083,"logicGr1",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr1,  // at (x,y,z)
                    logicGr1,     // its logical volume                                
                    "physigr1",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine prima slab della griglia dentro H2OBorata quarta parte del end

  // seconda slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr2x = (5.5/2.0)*mm;
  G4double Gr2y = 278.0*mm;
  G4double Gr2z = GrzComune;
  G4double PxGr2 = -100.0*mm;
  G4double PyGr2 = 0.0*mm;
  G4double PzGr2 = PzComune;
  G4ThreeVector positionGr2 = G4ThreeVector(PxGr2,PyGr2,PzGr2);
  G4Box* solidGr2 = new G4Box("solidGr2",Gr2x,Gr2y,Gr2z);
  logicGr2 = new G4LogicalVolume(solidGr2,AW5083,"logicGr2",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr2,  // at (x,y,z)
                    logicGr2,     // its logical volume                                
                    "physigr2",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine seconda slab della griglia dentro H2OBorata quarta parte del end

 // terza slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr3x = (5.5/2.0)*mm;
  G4double Gr3y = 215.0*mm;
  G4double Gr3z = GrzComune;
  G4double PxGr3 = -200.0*mm;
  G4double PyGr3 = 0.0*mm;
  G4double PzGr3 = PzComune;
  G4ThreeVector positionGr3 = G4ThreeVector(PxGr3,PyGr3,PzGr3);
  G4Box* solidGr3 = new G4Box("solidGr3",Gr3x,Gr3y,Gr3z);
  logicGr3 = new G4LogicalVolume(solidGr3,AW5083,"logicGr3",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr3,  // at (x,y,z)
                    logicGr3,     // its logical volume                                
                    "physigr3",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine terza slab della griglia dentro H2OBorata quarta parte del end

 // quarta slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr4x = (5.5/2.0)*mm;
  G4double Gr4y = 215.0*mm;
  G4double Gr4z = GrzComune;
  G4double PxGr4 = 200.0*mm;
  G4double PyGr4 = 0.0*mm;
  G4double PzGr4 = PzComune;
  G4ThreeVector positionGr4 = G4ThreeVector(PxGr4,PyGr4,PzGr4);
  G4Box* solidGr4 = new G4Box("solidGr4",Gr4x,Gr4y,Gr4z);
  logicGr4 = new G4LogicalVolume(solidGr4,AW5083,"logicGr4",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr4,  // at (x,y,z)
                    logicGr4,     // its logical volume                                
                    "physigr4",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine quarta slab della griglia dentro H2OBorata quarta parte del end

 // quinta slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr5x = (5.5/2.0)*mm;
  G4double Gr5y = 278.0*mm;
  G4double Gr5z = GrzComune;
  G4double PxGr5 = 100.0*mm;
  G4double PyGr5 = 0.0*mm;
  G4double PzGr5 = PzComune;
  G4ThreeVector positionGr5 = G4ThreeVector(PxGr5,PyGr5,PzGr5);
  G4Box* solidGr5 = new G4Box("solidGr5",Gr5x,Gr5y,Gr5z);
  logicGr5 = new G4LogicalVolume(solidGr5,AW5083,"logicGr5",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr5,  // at (x,y,z)
                    logicGr5,     // its logical volume                                
                    "physigr5",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine quinta slab della griglia dentro H2OBorata quarta parte del end

 // FINE SLAB LUNGO ASSE Y a diverse posizioni di X

  // INIZIO SLAB ORIZZONTALI LUNGO ASSE X (y = 0)

 // sesta slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr6x = (94.5/2.0)*mm;//era 15.0/2.0
  G4double Gr6y = (5.5/2.0)*mm;
  G4double Gr6z = GrzComune;
  G4double PxGr6 = ((5.5/2.0)+(94.5/2.0))*mm;
  G4double PyGr6 = 0.0*mm;
  G4double PzGr6 = PzComune;
  G4ThreeVector positionGr6 = G4ThreeVector(PxGr6,PyGr6,PzGr6);
  G4Box* solidGr6 = new G4Box("solidGr6",Gr6x,Gr6y,Gr6z);
  logicGr6 = new G4LogicalVolume(solidGr6,AW5083,"logicGr6",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr6,  // at (x,y,z)
                    logicGr6,     // its logical volume                                
                    "physigr6",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine sesta slab della griglia dentro H2OBorata quarta parte dell'end

 // settima slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr7x = (94.5/2.0)*mm;//era 15.0/2.0
  G4double Gr7y = (5.5/2.0)*mm;
  G4double Gr7z = GrzComune;
  G4double PxGr7 = -((5.5/2.0)+(94.5/2.0))*mm;
  G4double PyGr7 = 0.0*mm;
  G4double PzGr7 = PzComune;
  G4ThreeVector positionGr7 = G4ThreeVector(PxGr7,PyGr7,PzGr7);
  G4Box* solidGr7 = new G4Box("solidGr7",Gr7x,Gr7y,Gr7z);
  logicGr7 = new G4LogicalVolume(solidGr7,AW5083,"logicGr7",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr7,  // at (x,y,z)
                    logicGr7,     // its logical volume                                
                    "physigr7",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine settima slab della griglia dentro H2OBorata quarta parte dell'end

  /*
 // ottava slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr8x = (15.0/2.0)*mm;
  G4double Gr8y = (5.5/2.0)*mm;
  G4double Gr8z = GrzComune;
  G4double PxGr8 = (-100.0+(5.5/2.0)+(5.5/2.0)+(15.0/2.0))*mm;
  G4double PyGr8 = 0.0*mm;
  G4double PzGr8 = PzComune;
  G4ThreeVector positionGr8 = G4ThreeVector(PxGr8,PyGr8,PzGr8);
  G4Box* solidGr8 = new G4Box("solidGr8",Gr8x,Gr8y,Gr8z);
  logicGr8 = new G4LogicalVolume(solidGr8,AW5083,"logicGr8",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr8,  // at (x,y,z)
                    logicGr8,     // its logical volume                                
                    "physigr8",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine ottava slab della griglia dentro H2OBorata quarta parte dell'end
  */

 // nona slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr9x = (94.5/2.0)*mm;
  G4double Gr9y = (5.5/2.0)*mm;
  G4double Gr9z = GrzComune;
  G4double PxGr9 = (-94.5 - 3.0*(5.5/2.0) - (94.5/2.0))*mm;
  G4double PyGr9 = 0.0*mm;
  G4double PzGr9 = PzComune;
  G4ThreeVector positionGr9 = G4ThreeVector(PxGr9,PyGr9,PzGr9);
  G4Box* solidGr9 = new G4Box("solidGr9",Gr9x,Gr9y,Gr9z);
  logicGr9 = new G4LogicalVolume(solidGr9,AW5083,"logicGr9",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr9,  // at (x,y,z)
                    logicGr9,     // its logical volume                                
                    "physigr9",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine nona slab della griglia dentro H2OBorata quarta parte dell'end
  /*
 // decima slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr10x = (15.0/2.0)*mm;
  G4double Gr10y = (5.5/2.0)*mm;
  G4double Gr10z = GrzComune;
  G4double PxGr10 = (-200.0+(5.5/2.0)+(5.5/2.0)+(15.0/2.0))*mm;
  G4double PyGr10 = 0.0*mm;
  G4double PzGr10 = PzComune;
  G4ThreeVector positionGr10 = G4ThreeVector(PxGr10,PyGr10,PzGr10);
  G4Box* solidGr10 = new G4Box("solidGr10",Gr10x,Gr10y,Gr10z);
  logicGr10 = new G4LogicalVolume(solidGr10,AW5083,"logicGr10",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr10,  // at (x,y,z)
                    logicGr10,     // its logical volume                                
                    "physigr10",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine decima slab della griglia dentro H2OBorata quarta parte dell'end
  */
// undicesima slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr11x = (90.0/2.0)*mm;//94.5 non ci sta
  G4double Gr11y = (5.5/2.0)*mm;
  G4double Gr11z = GrzComune;
  G4double PxGr11 = -(94.5 + 3.0*(5.5/2.0) + 94.5 + 5.5 + (90.0/2.0))*mm;
  G4double PyGr11 = 0.0*mm;
  G4double PzGr11 = PzComune;
  G4ThreeVector positionGr11 = G4ThreeVector(PxGr11,PyGr11,PzGr11);
  G4Box* solidGr11 = new G4Box("solidGr11",Gr11x,Gr11y,Gr11z);
  logicGr11 = new G4LogicalVolume(solidGr11,AW5083,"logicGr11",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr11,  // at (x,y,z)
                    logicGr11,     // its logical volume                                
                    "physigr11",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine undicesima slab della griglia dentro H2OBorata quarta parte dell'end

  /*
  G4double Gr11bisx = (15.0/2.0)*mm;
  G4double Gr11bisy = (5.5/2.0)*mm;
  G4double Gr11bisz = GrzComune;
  G4double PxGr11bis = (-290.0)*mm;
  G4double PyGr11bis = 0.0*mm;
  G4double PzGr11bis = PzComune;
  G4ThreeVector positionGr11bis = G4ThreeVector(PxGr11bis,PyGr11bis,PzGr11bis);
  G4Box* solidGr11bis = new G4Box("solidGr11bis",Gr11bisx,Gr11bisy,Gr11bisz);
  logicGr11bis = new G4LogicalVolume(solidGr11bis,AW5083,"logicGr11bis",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr11bis,  // at (x,y,z)
                    logicGr11bis,     // its logical volume                                
                    "physigr11bis",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  */
  /*
 // dodicesima slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr12x = (15.0/2.0)*mm;
  G4double Gr12y = (5.5/2.0)*mm;
  G4double Gr12z = GrzComune;
  G4double PxGr12 = (100.0 - (5.5/2.0) - (5.5/2.0) - (15.0/2.0))*mm;
  G4double PyGr12 = 0.0*mm;
  G4double PzGr12 = PzComune;
  G4ThreeVector positionGr12 = G4ThreeVector(PxGr12,PyGr12,PzGr12);
  G4Box* solidGr12 = new G4Box("solidGr12",Gr12x,Gr12y,Gr12z);
  logicGr12 = new G4LogicalVolume(solidGr12,AW5083,"logicGr12",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr12,  // at (x,y,z)
                    logicGr12,     // its logical volume                                
                    "physigr12",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine dodicesima slab della griglia dentro H2OBorata quarta parte dell'end
  */

// tredicesima slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr13x = (94.5/2.0)*mm;
  G4double Gr13y = (5.5/2.0)*mm;
  G4double Gr13z = GrzComune;
  G4double PxGr13 = (94.5 + 3.0*(5.5/2.0) + (94.5/2.0))*mm;
  G4double PyGr13 = 0.0*mm;
  G4double PzGr13 = PzComune;
  G4ThreeVector positionGr13 = G4ThreeVector(PxGr13,PyGr13,PzGr13);
  G4Box* solidGr13 = new G4Box("solidGr13",Gr13x,Gr13y,Gr13z);
  logicGr13 = new G4LogicalVolume(solidGr13,AW5083,"logicGr13",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr13,  // at (x,y,z)
                    logicGr13,     // its logical volume                                
                    "physigr13",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine tredicesima slab della griglia dentro H2OBorata quarta parte dell'end

  /*
// quattordicesima slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr14x = (15.0/2.0)*mm;
  G4double Gr14y = (5.5/2.0)*mm;
  G4double Gr14z = GrzComune;
  G4double PxGr14 = (200.0 - (5.5/2.0) - (5.5/2.0) - (15.0/2.0))*mm;
  G4double PyGr14 = 0.0*mm;
  G4double PzGr14 = PzComune;
  G4ThreeVector positionGr14 = G4ThreeVector(PxGr14,PyGr14,PzGr14);
  G4Box* solidGr14 = new G4Box("solidGr14",Gr14x,Gr14y,Gr14z);
  logicGr14 = new G4LogicalVolume(solidGr14,AW5083,"logicGr14",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr14,  // at (x,y,z)
                    logicGr14,     // its logical volume                                
                    "physigr14",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine quattordicesima slab della griglia dentro H2OBorata quarta parte dell'end
  */
// quindicesima slab della griglia dentro H2OBorata quarta parte del end


  G4double Gr15x = (90.0/2.0)*mm;// 94.5 non ci sta
  G4double Gr15y = (5.5/2.0)*mm;
  G4double Gr15z = GrzComune;
  G4double PxGr15 = (94.5 + 3.0*(5.5/2.0) + 94.5 + 5.5 + (90.0/2.0))*mm;
  G4double PyGr15 = 0.0*mm;
  G4double PzGr15 = PzComune;
  G4ThreeVector positionGr15 = G4ThreeVector(PxGr15,PyGr15,PzGr15);
  G4Box* solidGr15 = new G4Box("solidGr15",Gr15x,Gr15y,Gr15z);
  logicGr15 = new G4LogicalVolume(solidGr15,AW5083,"logicGr15",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr15,  // at (x,y,z)
                    logicGr15,     // its logical volume                                
                    "physigr15",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  // fine quindicesima slab della griglia dentro H2OBorata quarta parte dell'end
  /*

  G4double Gr15bisx = (15.0/2.0)*mm;
  G4double Gr15bisy = (5.5/2.0)*mm;
  G4double Gr15bisz = GrzComune;
  G4double PxGr15bis = (290.0)*mm;
  G4double PyGr15bis = 0.0*mm;
  G4double PzGr15bis = PzComune;
  G4ThreeVector positionGr15bis = G4ThreeVector(PxGr15bis,PyGr15bis,PzGr15bis);
  G4Box* solidGr15bis = new G4Box("solidGr15",Gr15bisx,Gr15bisy,Gr15bisz);
  logicGr15bis = new G4LogicalVolume(solidGr15bis,AW5083,"logicGr15bis",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr15bis,  // at (x,y,z)
                    logicGr15bis,     // its logical volume                                
                    "physigr15bis",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  */
 // FINE SLAB ORIZZONTALI LUNGO ASSE X (y = 0)

  // INIZIO SLAB ORIZZONTALI LUNGO ASSE X (y = 10.75 cm)


  G4double Gr16x = (94.5/2.0)*mm;
  G4double Gr16y = (5.5/2.0)*mm;
  G4double Gr16z = GrzComune;
  G4double PxGr16 = ((5.5/2.0)+(94.5/2.0))*mm;
  G4double PyGr16 = (2.0*(5.5/2.0)+94.5)*mm;
  G4double PzGr16 = PzComune;
  G4ThreeVector positionGr16 = G4ThreeVector(PxGr16,PyGr16,PzGr16);
  G4Box* solidGr16 = new G4Box("solidGr6",Gr16x,Gr16y,Gr16z);
  logicGr16 = new G4LogicalVolume(solidGr16,AW5083,"logicGr16",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr16,  // at (x,y,z)
                    logicGr16,     // its logical volume                                
                    "physigr16",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 




  G4double Gr17x = (94.5/2.0)*mm;
  G4double Gr17y = (5.5/2.0)*mm;
  G4double Gr17z = GrzComune;
  G4double PxGr17 = -((5.5/2.0)+(94.5/2.0))*mm;
  G4double PyGr17 = (2.0*(5.5/2.0)+94.5)*mm;
  G4double PzGr17 = PzComune;
  G4ThreeVector positionGr17 = G4ThreeVector(PxGr17,PyGr17,PzGr17);
  G4Box* solidGr17 = new G4Box("solidGr17",Gr17x,Gr17y,Gr17z);
  logicGr17 = new G4LogicalVolume(solidGr17,AW5083,"logicGr17",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr17,  // at (x,y,z)
                    logicGr17,     // its logical volume                                
                    "physigr17",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 



  /*
  G4double Gr18x = (15.0/2.0)*mm;
  G4double Gr18y = (5.5/2.0)*mm;
  G4double Gr18z = GrzComune;
  G4double PxGr18 = (-100.0+(5.5/2.0)+(5.5/2.0)+(15.0/2.0))*mm;
  G4double PyGr18 =  107.5*mm;
  G4double PzGr18 = PzComune;
  G4ThreeVector positionGr18 = G4ThreeVector(PxGr18,PyGr18,PzGr18);
  G4Box* solidGr18 = new G4Box("solidGr18",Gr18x,Gr18y,Gr18z);
  logicGr18 = new G4LogicalVolume(solidGr18,AW5083,"logicGr18",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr18,  // at (x,y,z)
                    logicGr18,     // its logical volume                                
                    "physigr18",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 
  */

 
  G4double Gr19x = (94.5/2.0)*mm;
  G4double Gr19y = (5.5/2.0)*mm;
  G4double Gr19z = GrzComune;
  G4double PxGr19 =-(3.0*(5.5/2.0)+94.5 + 94.5/2.0)*mm;
  G4double PyGr19 = (2.0*(5.5/2.0)+94.5)*mm; 
  G4double PzGr19 = PzComune;
  G4ThreeVector positionGr19 = G4ThreeVector(PxGr19,PyGr19,PzGr19);
  G4Box* solidGr19 = new G4Box("solidGr19",Gr19x,Gr19y,Gr19z);
  logicGr19 = new G4LogicalVolume(solidGr19,AW5083,"logicGr19",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr19,  // at (x,y,z)
                    logicGr19,     // its logical volume                                
                    "physigr19",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 



  /*
  G4double Gr20x = (15.0/2.0)*mm;
  G4double Gr20y = (5.5/2.0)*mm;
  G4double Gr20z = GrzComune;
  G4double PxGr20 = (-200.0+(5.5/2.0)+(5.5/2.0)+(15.0/2.0))*mm;
  G4double PyGr20 =  107.5*mm;
  G4double PzGr20 = PzComune;
  G4ThreeVector positionGr20 = G4ThreeVector(PxGr20,PyGr20,PzGr20);
  G4Box* solidGr20 = new G4Box("solidGr10",Gr20x,Gr20y,Gr20z);
  logicGr20 = new G4LogicalVolume(solidGr20,AW5083,"logicGr20",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr20,  // at (x,y,z)
                    logicGr20,     // its logical volume                                
                    "physigr20",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  */

  G4double Gr21x = (72.0/2.0)*mm;//94.5 non ci sta
  G4double Gr21y = (5.5/2.0)*mm;
  G4double Gr21z = GrzComune;
  G4double PxGr21 =  -(94.5 + 3.0*(5.5/2.0) + 94.5 + 5.5 + (72.0/2.0))*mm;
  G4double PyGr21 =   (2.0*(5.5/2.0)+94.5)*mm; 
  G4double PzGr21 = PzComune;
  G4ThreeVector positionGr21 = G4ThreeVector(PxGr21,PyGr21,PzGr21);
  G4Box* solidGr21 = new G4Box("solidGr21",Gr21x,Gr21y,Gr21z);
  logicGr21 = new G4LogicalVolume(solidGr21,AW5083,"logicGr21",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr21,  // at (x,y,z)
                    logicGr21,     // its logical volume                                
                    "physigr21",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  /*

  G4double Gr22x = (15.0/2.0)*mm;
  G4double Gr22y = (5.5/2.0)*mm;
  G4double Gr22z = GrzComune;
  G4double PxGr22 = (100.0 - (5.5/2.0) - (5.5/2.0) - (15.0/2.0))*mm;
  G4double PyGr22 =  107.5*mm;
  G4double PzGr22 = PzComune;
  G4ThreeVector positionGr22 = G4ThreeVector(PxGr22,PyGr22,PzGr22);
  G4Box* solidGr22 = new G4Box("solidGr22",Gr22x,Gr22y,Gr22z);
  logicGr22 = new G4LogicalVolume(solidGr22,AW5083,"logicGr22",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr22,  // at (x,y,z)
                    logicGr22,     // its logical volume                                
                    "physigr22",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  */

  
  G4double Gr23x = (94.5/2.0)*mm;
  G4double Gr23y = (5.5/2.0)*mm;
  G4double Gr23z = GrzComune;
  G4double PxGr23 = (3.0*(5.5/2.0)+94.5 + 94.5/2.0)*mm;
  G4double PyGr23 = (2.0*(5.5/2.0)+94.5)*mm;
  G4double PzGr23 = PzComune;
  G4ThreeVector positionGr23 = G4ThreeVector(PxGr23,PyGr23,PzGr23);
  G4Box* solidGr23 = new G4Box("solidGr23",Gr23x,Gr23y,Gr23z);
  logicGr23 = new G4LogicalVolume(solidGr23,AW5083,"logicGr23",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr23,  // at (x,y,z)
                    logicGr23,     // its logical volume                                
                    "physigr23",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  /*
  G4double Gr24x = (15.0/2.0)*mm;
  G4double Gr24y = (5.5/2.0)*mm;
  G4double Gr24z = GrzComune;
  G4double PxGr24 = (200.0 - (5.5/2.0) - (5.5/2.0) - (15.0/2.0))*mm;
  G4double PyGr24 =  107.5*mm;
  G4double PzGr24 = PzComune;
  G4ThreeVector positionGr24 = G4ThreeVector(PxGr24,PyGr24,PzGr24);
  G4Box* solidGr24 = new G4Box("solidGr24",Gr24x,Gr24y,Gr24z);
  logicGr24 = new G4LogicalVolume(solidGr24,AW5083,"logicGr24",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr24,  // at (x,y,z)
                    logicGr24,     // its logical volume                                
                    "physigr24",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 
  */


  

  G4double Gr25x = (72.0/2.0)*mm;
  G4double Gr25y = (5.5/2.0)*mm;
  G4double Gr25z = GrzComune;
  G4double PxGr25 = (94.5 + 3.0*(5.5/2.0) + 94.5 + 5.5 + (72.0/2.0))*mm;
  G4double PyGr25 = (2.0*(5.5/2.0)+94.5)*mm; 
  G4double PzGr25 = PzComune;
  G4ThreeVector positionGr25 = G4ThreeVector(PxGr25,PyGr25,PzGr25);
  G4Box* solidGr25 = new G4Box("solidGr25",Gr25x,Gr25y,Gr25z);
  logicGr25 = new G4LogicalVolume(solidGr25,AW5083,"logicGr25",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr25,  // at (x,y,z)
                    logicGr25,     // its logical volume                                
                    "physigr25",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  
 // FINE SLAB ORIZZONTALI LUNGO ASSE X (y = 10.75cm)

  // INIZIO SLAB ORIZZONTALI LUNGO ASSE X (y = -10.75 cm)

 

  G4double Gr26x = (94.5/2.0)*mm;
  G4double Gr26y = (5.5/2.0)*mm;
  G4double Gr26z = GrzComune;
  G4double PxGr26 =  ((5.5/2.0)+(94.5/2.0))*mm;
  G4double PyGr26 =  -(2.0*(5.5/2.0)+94.5)*mm;
  G4double PzGr26 = PzComune;
  G4ThreeVector positionGr26 = G4ThreeVector(PxGr26,PyGr26,PzGr26);
  G4Box* solidGr26 = new G4Box("solidGr6",Gr26x,Gr26y,Gr26z);
  logicGr26 = new G4LogicalVolume(solidGr26,AW5083,"logicGr26",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr26,  // at (x,y,z)
                    logicGr26,     // its logical volume                                
                    "physigr26",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 




  G4double Gr27x = (94.5/2.0)*mm;
  G4double Gr27y = (5.5/2.0)*mm;
  G4double Gr27z = GrzComune;
  G4double PxGr27 =  -((5.5/2.0)+(94.5/2.0))*mm;
  G4double PyGr27 =  -(2.0*(5.5/2.0)+94.5)*mm;
  G4double PzGr27 = PzComune;
  G4ThreeVector positionGr27 = G4ThreeVector(PxGr27,PyGr27,PzGr27);
  G4Box* solidGr27 = new G4Box("solidGr27",Gr27x,Gr27y,Gr27z);
  logicGr27 = new G4LogicalVolume(solidGr27,AW5083,"logicGr27",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr27,  // at (x,y,z)
                    logicGr27,     // its logical volume                                
                    "physigr27",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 



  /*
  G4double Gr28x = (15.0/2.0)*mm;
  G4double Gr28y = (5.5/2.0)*mm;
  G4double Gr28z = GrzComune;
  G4double PxGr28 = (-100.0+(5.5/2.0)+(5.5/2.0)+(15.0/2.0))*mm;
  G4double PyGr28 =  -107.5*mm;
  G4double PzGr28 = PzComune;
  G4ThreeVector positionGr28 = G4ThreeVector(PxGr28,PyGr28,PzGr28);
  G4Box* solidGr28 = new G4Box("solidGr28",Gr28x,Gr28y,Gr28z);
  logicGr28 = new G4LogicalVolume(solidGr28,AW5083,"logicGr28",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr28,  // at (x,y,z)
                    logicGr28,     // its logical volume                                
                    "physigr28",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  */

 

  G4double Gr29x = (94.5/2.0)*mm;
  G4double Gr29y = (5.5/2.0)*mm;
  G4double Gr29z = GrzComune;
  G4double PxGr29 =-(3.0*(5.5/2.0)+94.5 + 94.5/2.0)*mm;
  G4double PyGr29 =-(2.0*(5.5/2.0)+94.5)*mm; 
  G4double PzGr29 = PzComune;
  G4ThreeVector positionGr29 = G4ThreeVector(PxGr29,PyGr29,PzGr29);
  G4Box* solidGr29 = new G4Box("solidGr29",Gr29x,Gr29y,Gr29z);
  logicGr29 = new G4LogicalVolume(solidGr29,AW5083,"logicGr29",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr29,  // at (x,y,z)
                    logicGr29,     // its logical volume                                
                    "physigr29",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 



  /*
  G4double Gr30x = (15.0/2.0)*mm;
  G4double Gr30y = (5.5/2.0)*mm;
  G4double Gr30z = GrzComune;
  G4double PxGr30 = (-200.0+(5.5/2.0)+(5.5/2.0)+(15.0/2.0))*mm;
  G4double PyGr30 =  -107.5*mm;
  G4double PzGr30 = PzComune;
  G4ThreeVector positionGr30 = G4ThreeVector(PxGr30,PyGr30,PzGr30);
  G4Box* solidGr30 = new G4Box("solidGr10",Gr30x,Gr30y,Gr30z);
  logicGr30 = new G4LogicalVolume(solidGr30,AW5083,"logicGr30",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr30,  // at (x,y,z)
                    logicGr30,     // its logical volume                                
                    "physigr30",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 
  */


  

  G4double Gr31x = (72.0/2.0)*mm;
  G4double Gr31y = (5.5/2.0)*mm;
  G4double Gr31z = GrzComune;
  G4double PxGr31 =-(94.5 + 3.0*(5.5/2.0) + 94.5 + 5.5 + (72.0/2.0))*mm;
  G4double PyGr31 =-(2.0*(5.5/2.0)+94.5)*mm;    
  G4double PzGr31 = PzComune;
  G4ThreeVector positionGr31 = G4ThreeVector(PxGr31,PyGr31,PzGr31);
  G4Box* solidGr31 = new G4Box("solidGr31",Gr31x,Gr31y,Gr31z);
  logicGr31 = new G4LogicalVolume(solidGr31,AW5083,"logicGr31",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr31,  // at (x,y,z)
                    logicGr31,     // its logical volume                                
                    "physigr31",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  /*
  G4double Gr32x = (15.0/2.0)*mm;
  G4double Gr32y = (5.5/2.0)*mm;
  G4double Gr32z = GrzComune;
  G4double PxGr32 = (100.0 - (5.5/2.0) - (5.5/2.0) - (15.0/2.0))*mm;
  G4double PyGr32 =  -107.5*mm;
  G4double PzGr32 = PzComune;
  G4ThreeVector positionGr32 = G4ThreeVector(PxGr32,PyGr32,PzGr32);
  G4Box* solidGr32 = new G4Box("solidGr32",Gr32x,Gr32y,Gr32z);
  logicGr32 = new G4LogicalVolume(solidGr32,AW5083,"logicGr32",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr32,  // at (x,y,z)
                    logicGr32,     // its logical volume                                
                    "physigr32",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  */

 
  G4double Gr33x = (94.5/2.0)*mm;
  G4double Gr33y = (5.5/2.0)*mm;
  G4double Gr33z = GrzComune;
  G4double PxGr33 = (3.0*(5.5/2.0)+94.5 + 94.5/2.0)*mm;
  G4double PyGr33 = -(2.0*(5.5/2.0)+94.5)*mm;
  G4double PzGr33 = PzComune;
  G4ThreeVector positionGr33 = G4ThreeVector(PxGr33,PyGr33,PzGr33);
  G4Box* solidGr33 = new G4Box("solidGr33",Gr33x,Gr33y,Gr33z);
  logicGr33 = new G4LogicalVolume(solidGr33,AW5083,"logicGr33",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr33,  // at (x,y,z)
                    logicGr33,     // its logical volume                                
                    "physigr33",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  /*
  G4double Gr34x = (15.0/2.0)*mm;
  G4double Gr34y = (5.5/2.0)*mm;
  G4double Gr34z = GrzComune;
  G4double PxGr34 = (200.0 - (5.5/2.0) - (5.5/2.0) - (15.0/2.0))*mm;
  G4double PyGr34 =  -107.5*mm;
  G4double PzGr34 = PzComune;
  G4ThreeVector positionGr34 = G4ThreeVector(PxGr34,PyGr34,PzGr34);
  G4Box* solidGr34 = new G4Box("solidGr34",Gr34x,Gr34y,Gr34z);
  logicGr34 = new G4LogicalVolume(solidGr34,AW5083,"logicGr34",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr34,  // at (x,y,z)
                    logicGr34,     // its logical volume                                
                    "physigr34",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 
  */




  G4double Gr35x = (72.0/2.0)*mm;
  G4double Gr35y = (5.5/2.0)*mm;
  G4double Gr35z = GrzComune;
  G4double PxGr35 = (94.5 + 3.0*(5.5/2.0) + 94.5 + 5.5 + (72.0/2.0))*mm;
  G4double PyGr35 =    -(2.0*(5.5/2.0)+94.5)*mm;   
  G4double PzGr35 = PzComune;
  G4ThreeVector positionGr35 = G4ThreeVector(PxGr35,PyGr35,PzGr35);
  G4Box* solidGr35 = new G4Box("solidGr35",Gr35x,Gr35y,Gr35z);
  logicGr35 = new G4LogicalVolume(solidGr35,AW5083,"logicGr35",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr35,  // at (x,y,z)
                    logicGr35,     // its logical volume                                
                    "physigr35",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  
 // FINE SLAB ORIZZONTALI LUNGO ASSE X (y = -10.75cm)

  // INIZIO SLAB ORIZZONTALI LUNGO ASSE X (y = -21.225 cm)
 
  G4double Gr36x = (94.5/2.0)*mm;
  G4double Gr36y = (5.5/2.0)*mm;
  G4double Gr36z = GrzComune;
  G4double PxGr36 = ((5.5/2.0)+(94.5/2.0))*mm;
  G4double PyGr36 = -(4.0*(5.5/2.0)+2.0*94.5)*mm;
  G4double PzGr36 = PzComune;
  G4ThreeVector positionGr36 = G4ThreeVector(PxGr36,PyGr36,PzGr36);
  G4Box* solidGr36 = new G4Box("solidGr6",Gr36x,Gr36y,Gr36z);
  logicGr36 = new G4LogicalVolume(solidGr36,AW5083,"logicGr36",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr36,  // at (x,y,z)
                    logicGr36,     // its logical volume                                
                    "physigr36",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


 
  G4double Gr37x = (94.5/2.0)*mm;
  G4double Gr37y = (5.5/2.0)*mm;
  G4double Gr37z = GrzComune;
  G4double PxGr37 = -((5.5/2.0)+(94.5/2.0))*mm;
  G4double PyGr37 =  -(4.0*(5.5/2.0)+2.0*94.5)*mm;
  G4double PzGr37 = PzComune;
  G4ThreeVector positionGr37 = G4ThreeVector(PxGr37,PyGr37,PzGr37);
  G4Box* solidGr37 = new G4Box("solidGr37",Gr37x,Gr37y,Gr37z);
  logicGr37 = new G4LogicalVolume(solidGr37,AW5083,"logicGr37",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr37,  // at (x,y,z)
                    logicGr37,     // its logical volume                                
                    "physigr37",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 



  /*
  G4double Gr38x = (15.0/2.0)*mm;
  G4double Gr38y = (5.5/2.0)*mm;
  G4double Gr38z = GrzComune;
  G4double PxGr38 = (-100.0+(5.5/2.0)+(5.5/2.0)+(15.0/2.0))*mm;
  G4double PyGr38 =  -212.25*mm;
  G4double PzGr38 = PzComune;
  G4ThreeVector positionGr38 = G4ThreeVector(PxGr38,PyGr38,PzGr38);
  G4Box* solidGr38 = new G4Box("solidGr38",Gr38x,Gr38y,Gr38z);
  logicGr38 = new G4LogicalVolume(solidGr38,AW5083,"logicGr38",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr38,  // at (x,y,z)
                    logicGr38,     // its logical volume                                
                    "physigr38",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  */

 
  G4double Gr39x = (94.5/2.0)*mm;
  G4double Gr39y = (5.5/2.0)*mm;
  G4double Gr39z = GrzComune;
  G4double PxGr39 =-(3.0*(5.5/2.0)+ 94.5 +(94.5/2.0))*mm;  
  G4double PyGr39 = -(4.0*(5.5/2.0)+2.0*94.5)*mm;   
  G4double PzGr39 = PzComune;
  G4ThreeVector positionGr39 = G4ThreeVector(PxGr39,PyGr39,PzGr39);
  G4Box* solidGr39 = new G4Box("solidGr39",Gr39x,Gr39y,Gr39z);
  logicGr39 = new G4LogicalVolume(solidGr39,AW5083,"logicGr39",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr39,  // at (x,y,z)
                    logicGr39,     // its logical volume                                
                    "physigr39",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 



  /*
  G4double Gr40x = (15.0/2.0)*mm;
  G4double Gr40y = (5.5/2.0)*mm;
  G4double Gr40z = GrzComune;
  G4double PxGr40 = (-200.0+(5.5/2.0)+(5.5/2.0)+(15.0/2.0))*mm;
  G4double PyGr40 =  -212.25*mm;
  G4double PzGr40 = PzComune;
  G4ThreeVector positionGr40 = G4ThreeVector(PxGr40,PyGr40,PzGr40);
  G4Box* solidGr40 = new G4Box("solidGr10",Gr40x,Gr40y,Gr40z);
  logicGr40 = new G4LogicalVolume(solidGr40,AW5083,"logicGr40",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr40,  // at (x,y,z)
                    logicGr40,     // its logical volume                                
                    "physigr40",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  */

  /*
  G4double Gr41x = (15.0/2.0)*mm;
  G4double Gr41y = (5.5/2.0)*mm;
  G4double Gr41z = GrzComune;
  G4double PxGr41 = (100.0 - (5.5/2.0) - (5.5/2.0) - (15.0/2.0))*mm;
  G4double PyGr41 =  -212.25*mm;
  G4double PzGr41 = PzComune;
  G4ThreeVector positionGr41 = G4ThreeVector(PxGr41,PyGr41,PzGr41);
  G4Box* solidGr41 = new G4Box("solidGr41",Gr41x,Gr41y,Gr41z);
  logicGr41 = new G4LogicalVolume(solidGr41,AW5083,"logicGr41",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr41,  // at (x,y,z)
                    logicGr41,     // its logical volume                                
                    "physigr41",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 
  */


  G4double Gr42x = (94.5/2.0)*mm;
  G4double Gr42y = (5.5/2.0)*mm;
  G4double Gr42z = GrzComune;
  G4double PxGr42 = (3.0*(5.5/2.0)+ 94.5 +(94.5/2.0))*mm;
  G4double PyGr42 = -(4.0*(5.5/2.0)+2.0*94.5)*mm;
  G4double PzGr42 = PzComune;
  G4ThreeVector positionGr42 = G4ThreeVector(PxGr42,PyGr42,PzGr42);
  G4Box* solidGr42 = new G4Box("solidGr42",Gr42x,Gr42y,Gr42z);
  logicGr42 = new G4LogicalVolume(solidGr42,AW5083,"logicGr42",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr42,  // at (x,y,z)
                    logicGr42,     // its logical volume                                
                    "physigr42",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  /*

  G4double Gr43x = (15.0/2.0)*mm;
  G4double Gr43y = (5.5/2.0)*mm;
  G4double Gr43z = GrzComune;
  G4double PxGr43 = (200.0 - (5.5/2.0) - (5.5/2.0) - (15.0/2.0))*mm;
  G4double PyGr43 =  -212.25*mm;
  G4double PzGr43 = PzComune;
  G4ThreeVector positionGr43 = G4ThreeVector(PxGr43,PyGr43,PzGr43);
  G4Box* solidGr43 = new G4Box("solidGr43",Gr43x,Gr43y,Gr43z);
  logicGr43 = new G4LogicalVolume(solidGr43,AW5083,"logicGr43",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr43,  // at (x,y,z)
                    logicGr43,     // its logical volume                                
                    "physigr43",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  */
 // FINE SLAB ORIZZONTALI LUNGO ASSE X (y = -21.225cm)

  // INIZIO SLAB ORIZZONTALI LUNGO ASSE X (y = 21.225 cm)


 
  G4double Gr44x = (94.5/2.0)*mm;
  G4double Gr44y = (5.5/2.0)*mm;
  G4double Gr44z = GrzComune;
  G4double PxGr44 = ((5.5/2.0)+(94.5/2.0))*mm;
  G4double PyGr44 = (4.0*(5.5/2.0)+2.0*94.5)*mm;
  G4double PzGr44 = PzComune;
  G4ThreeVector positionGr44 = G4ThreeVector(PxGr44,PyGr44,PzGr44);
  G4Box* solidGr44 = new G4Box("solidGr6",Gr44x,Gr44y,Gr44z);
  logicGr44 = new G4LogicalVolume(solidGr44,AW5083,"logicGr44",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr44,  // at (x,y,z)
                    logicGr44,     // its logical volume                                
                    "physigr44",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 




  G4double Gr45x = (94.5/2.0)*mm;
  G4double Gr45y = (5.5/2.0)*mm;
  G4double Gr45z = GrzComune;
  G4double PxGr45 = -((5.5/2.0)+(94.5/2.0))*mm;
  G4double PyGr45 = (4.0*(5.5/2.0)+2.0*94.5)*mm;
  G4double PzGr45 = PzComune;
  G4ThreeVector positionGr45 = G4ThreeVector(PxGr45,PyGr45,PzGr45);
  G4Box* solidGr45 = new G4Box("solidGr45",Gr45x,Gr45y,Gr45z);
  logicGr45 = new G4LogicalVolume(solidGr45,AW5083,"logicGr45",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr45,  // at (x,y,z)
                    logicGr45,     // its logical volume                                
                    "physigr45",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 



  /*
  G4double Gr46x = (15.0/2.0)*mm;
  G4double Gr46y = (5.5/2.0)*mm;
  G4double Gr46z = GrzComune;
  G4double PxGr46 = (-100.0+(5.5/2.0)+(5.5/2.0)+(15.0/2.0))*mm;
  G4double PyGr46 =  212.25*mm;
  G4double PzGr46 = PzComune;
  G4ThreeVector positionGr46 = G4ThreeVector(PxGr46,PyGr46,PzGr46);
  G4Box* solidGr46 = new G4Box("solidGr46",Gr46x,Gr46y,Gr46z);
  logicGr46 = new G4LogicalVolume(solidGr46,AW5083,"logicGr46",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr46,  // at (x,y,z)
                    logicGr46,     // its logical volume                                
                    "physigr46",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  */

 
  G4double Gr47x = (94.5/2.0)*mm;
  G4double Gr47y = (5.5/2.0)*mm;
  G4double Gr47z = GrzComune;
  G4double PxGr47 =-(3.0*(5.5/2.0)+ 94.5 +(94.5/2.0))*mm; 
  G4double PyGr47 = (4.0*(5.5/2.0)+2.0*94.5)*mm;  
  G4double PzGr47 = PzComune;
  G4ThreeVector positionGr47 = G4ThreeVector(PxGr47,PyGr47,PzGr47);
  G4Box* solidGr47 = new G4Box("solidGr47",Gr47x,Gr47y,Gr47z);
  logicGr47 = new G4LogicalVolume(solidGr47,AW5083,"logicGr47",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr47,  // at (x,y,z)
                    logicGr47,     // its logical volume                                
                    "physigr47",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 



  /*
  G4double Gr48x = (15.0/2.0)*mm;
  G4double Gr48y = (5.5/2.0)*mm;
  G4double Gr48z = GrzComune;
  G4double PxGr48 = (-200.0+(5.5/2.0)+(5.5/2.0)+(15.0/2.0))*mm;
  G4double PyGr48 =  212.25*mm;
  G4double PzGr48 = PzComune;
  G4ThreeVector positionGr48 = G4ThreeVector(PxGr48,PyGr48,PzGr48);
  G4Box* solidGr48 = new G4Box("solidGr10",Gr48x,Gr48y,Gr48z);
  logicGr48 = new G4LogicalVolume(solidGr48,AW5083,"logicGr48",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr48,  // at (x,y,z)
                    logicGr48,     // its logical volume                                
                    "physigr48",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 
  */


  /*
  G4double Gr49x = (15.0/2.0)*mm;
  G4double Gr49y = (5.5/2.0)*mm;
  G4double Gr49z = GrzComune;
  G4double PxGr49 = (100.0 - (5.5/2.0) - (5.5/2.0) - (15.0/2.0))*mm;
  G4double PyGr49 =  212.25*mm;
  G4double PzGr49 = PzComune;
  G4ThreeVector positionGr49 = G4ThreeVector(PxGr49,PyGr49,PzGr49);
  G4Box* solidGr49 = new G4Box("solidGr49",Gr49x,Gr49y,Gr49z);
  logicGr49 = new G4LogicalVolume(solidGr49,AW5083,"logicGr49",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr49,  // at (x,y,z)
                    logicGr49,     // its logical volume                                
                    "physigr49",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  */

  
  

  G4double Gr50x = (94.5/2.0)*mm;
  G4double Gr50y = (5.5/2.0)*mm;
  G4double Gr50z = GrzComune;
  G4double PxGr50 = (3.0*(5.5/2.0)+ 94.5 +(94.5/2.0))*mm;
  G4double PyGr50 = (4.0*(5.5/2.0)+2.0*94.5)*mm;
  G4double PzGr50 = PzComune;
  G4ThreeVector positionGr50 = G4ThreeVector(PxGr50,PyGr50,PzGr50);
  G4Box* solidGr50 = new G4Box("solidGr50",Gr50x,Gr50y,Gr50z);
  logicGr50 = new G4LogicalVolume(solidGr50,AW5083,"logicGr50",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr50,  // at (x,y,z)
                    logicGr50,     // its logical volume                                
                    "physigr50",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 


  /*
  G4double Gr51x = (15.0/2.0)*mm;
  G4double Gr51y = (5.5/2.0)*mm;
  G4double Gr51z = GrzComune;
  G4double PxGr51 = (200.0 - (5.5/2.0) - (5.5/2.0) - (15.0/2.0))*mm;
  G4double PyGr51 =  212.25*mm;
  G4double PzGr51 = PzComune;
  G4ThreeVector positionGr51 = G4ThreeVector(PxGr51,PyGr51,PzGr51);
  G4Box* solidGr51 = new G4Box("solidGr51",Gr51x,Gr51y,Gr51z);
  logicGr51 = new G4LogicalVolume(solidGr51,AW5083,"logicGr51",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionGr51,  // at (x,y,z)
                    logicGr51,     // its logical volume                                
                    "physigr51",        // its name
                    logicEnd4H2OBSpa,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  */
 // FINE SLAB ORIZZONTALI LUNGO ASSE X (y = 21.225cm)



 // Volume di aria verso EAR1 

  G4double AirEndZ = (28.0/2.0)*mm;
  G4double AirEndRest = 350.0*mm;
  G4double AirEndPz = (AlSpaZ + 2.0*End1AlSpaZ + AirEndZ)*mm; 
  G4ThreeVector positionAirEnd = G4ThreeVector(0.0,0.0, AirEndPz);
  G4Tubs* solidAirEnd = new G4Tubs("solidAirEnd",0.0,AirEndRest,AirEndZ,0.,twopi);
  G4Material *air = G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR");
  logicAirEnd = new G4LogicalVolume(solidAirEnd,
                                    G4NistManager::Instance()->BuildMaterialWithNewDensity("HotAir", "G4_AIR", air->GetDensity(), temperature),
                                    "logicAirEnd",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionAirEnd,  // at (x,y,z)
		    logicAirEnd,     // its logical volume				  
		    "physiAirEnd",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine volume aria verso EAR1 ---------------------------------------------------------------



 // Slab di 1.6 mm verso EAR1 

  G4double GrigliaEndZ = (1.6/2.0)*mm;//era 7.0/2.0
  G4double GrigliaEndRest = 350.0*mm;
  G4double GrigliaEndPz = (AlSpaZ + 2.0*End1AlSpaZ + 2.0*AirEndZ + GrigliaEndZ)*mm; 
  G4ThreeVector positionGrigliaEnd = G4ThreeVector(0.0,0.0, GrigliaEndPz);
  G4Tubs* solidGrigliaEnd = new G4Tubs("solidGrigliaEnd",0.0,GrigliaEndRest,GrigliaEndZ,0.,twopi);
  logicGrigliaEnd = new G4LogicalVolume(solidGrigliaEnd,AW5083,"logicGrigliaEnd",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionGrigliaEnd,  // at (x,y,z)
		    logicGrigliaEnd,     // its logical volume				  
		    "physiGrigliaEnd",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine slab di 1.6mm verso EAR1 ---------------------------------------------------------------



  // Box 2 di Al in cui verrà inserita la griglia finale 
 // PROVA ROTAZIONE 
 G4RotationMatrix* zRot = new G4RotationMatrix;
 zRot->rotateZ((pi/4.0)*rad);

  G4double Griglia2EndZ = (50.0/2.0)*mm;
  G4double Griglia2EndX = (800.0/2.0)*mm; 
  G4double Griglia2EndY = (800.0/2.0)*mm; 
  G4double Griglia2EndPz = (AlSpaZ + 2.0*End1AlSpaZ + 2.0*AirEndZ + 2.0*GrigliaEndZ + Griglia2EndZ)*mm; 
  G4ThreeVector positionGriglia2End = G4ThreeVector(0.0,0.0, Griglia2EndPz);

  G4Box* boxGriglia2End   = new G4Box("boxGriglia2End",Griglia2EndX,Griglia2EndY,Griglia2EndZ); 

  logicGriglia2End = new G4LogicalVolume(boxGriglia2End,AW5083,"logicGriglia2End");
  new G4PVPlacement(zRot,               // no rotation
		    positionGriglia2End,  // at (x,y,z)
		    logicGriglia2End,     // its logical volume				  
		    "physiGriglia2End",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 
 
  // fine Box 2 di Al in cui verrà inserita la griglia finale 


  //
  // Check volume
  //
 

  G4int nPixel = 49;
  G4double pitchPixel = 105.5*mm; 
  
  G4double PixelGrid2_x = (100.0/2.0)*mm ; 
  G4double PixelGrid2_y = (100.0/2.0)*mm ; 
  G4double PixelGrid2_z = (50.0/2.0)*mm ; 
  G4double posF_x = -316.5*mm;
  
  G4Box* PixelGrid2 = new G4Box("PixelGrid2", PixelGrid2_x ,PixelGrid2_y ,PixelGrid2_z); 
  
  
  logicPixelGrid2 = new G4LogicalVolume(PixelGrid2, galactic,"logicPixelGrid2");

  
  G4VPVParameterisation* checkParam = new ChamberParameterisation(
								  nPixel, // number of tubes
								  posF_x, // X of first tube
								  pitchPixel); // Pitch
  
  Grid2 = new G4PVParameterised(
				"Grid2", // name
				logicPixelGrid2, // logical volume
				logicGriglia2End, // mother logical volume
				kZAxis, // are placed along Z axis
				nPixel, // number of copies
				checkParam); // tha parametrisation



 // vuoto prima del dummy EAR1 

  G4double Dummy2VuotoZ = (35.0/2.0)*mm;
  G4double Dummy2VuotoRest = 350.0*mm;
  G4double Dummy2VuotoPz = (Griglia2EndPz + Griglia2EndZ + Dummy2VuotoZ)*mm; 
  G4ThreeVector positionDummy2Vuoto = G4ThreeVector(0.0,0.0, Dummy2VuotoPz);
  G4Tubs* solidDummy2Vuoto = new G4Tubs("solidDummy2Vuoto",0.0,Dummy2VuotoRest,Dummy2VuotoZ,0.,twopi);
  logicDummy2Vuoto = new G4LogicalVolume(solidDummy2Vuoto, galactic,"logicDummy2Vuoto",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionDummy2Vuoto,  // at (x,y,z)
		    logicDummy2Vuoto,     // its logical volume				  
		    "physiDum2Vuoto",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine vuoto prima del dummy EAR1 ---------------------------------------------------------------


 // Detector dummy (physiDum2) EAR1 

  G4double Dummy2Z = (2.0/2.0)*mm;
  G4double Dummy2Rest = 350.0*mm;
  G4double Dummy2Pz = (Dummy2VuotoPz + Dummy2VuotoZ + Dummy2Z)*mm; 
  G4ThreeVector positionDummy2 = G4ThreeVector(0.0,0.0, Dummy2Pz);
  G4Tubs* solidDummy2 = new G4Tubs("solidDummy2",0.0,Dummy2Rest,Dummy2Z,0.,twopi);
  logicDummy2 = new G4LogicalVolume(solidDummy2, galactic,"logicDummy2",0,0,0);
  new G4PVPlacement(0,               // no rotation
		    positionDummy2,  // at (x,y,z)
		    logicDummy2,     // its logical volume				  
		    "physiDum2",        // its name
		    logicWorld,      // its mother  volume
		    false,           // no boolean operations
		    0);              // no particular field 


  // fine Detector dummy EAR1 ---------------------------------------------------------------


  
  //-------------------------------------------------------
  // detector dummy2UP

 
  G4double Dum2UPy = 1.0*mm;
  G4double Dum2UPx = (380.0/2.0)*mm;//era 150
  G4double Dum2UPz = (380.0/2.0)*mm;// era 150
  G4double PyDum2UP = (AlSpaR + Dum2UPy)*mm;
  G4ThreeVector positionDum2UP = G4ThreeVector(0.0,PyDum2UP,0.0);
  G4Box* solidDum2UP = new G4Box("solidDum2UP",Dum2UPx,Dum2UPy,Dum2UPz);
  logicDum2UP = new G4LogicalVolume(solidDum2UP,galactic,"logicDum2UP",0,0,0);
  new G4PVPlacement(0,               // no rotation
                    positionDum2UP,  // at (x,y,z)
                    logicDum2UP,     // its logical volume                                
                    "physiDum2UP",        // its name
                    logicWorld,      // its mother  volume
                    false,           // no boolean operations
                    0);              // no particular field 

  //-------------------------------------------------------

  
                           
  PrintParameters();

  

  //----------------------------------------------------------------------
  // ATTENZIONE!!!!!!!

  // Sfera "Killing" necessaria ad uccidere le particelle che si avvicinano
  // ad OutWorld. La presenza del PostStep nello SteppingAction determina il 
  // segmentation fault quando ci si avvicina al OutWorld

 
  G4double r_est = 300.0*m;
  G4double r_int = 299.99*m;
  
  G4Sphere* solidKilling = new G4Sphere("solidKilling", r_int, r_est, 0., twopi, 0., twopi);

  
  logicKilling = new G4LogicalVolume(solidKilling,
				    galactic, "logicKilling");
  
  new G4PVPlacement(0,G4ThreeVector(0., 0., 0.),logicKilling,
		    "physiKilling",logicWorld,false,0);
  


 // colors
 
  logicWorld->SetVisAttributes(G4VisAttributes::Invisible);
  logicKilling->SetVisAttributes(G4VisAttributes::Invisible);

  G4VisAttributes* redcolor = new G4VisAttributes(G4Colour(1.0, 0.0, 0.0));//red
  G4VisAttributes* blucolor = new G4VisAttributes(G4Colour(0.0, 0.0, 1.0));//blu
  G4VisAttributes* verdecolor = new G4VisAttributes(G4Colour(0.0, 1.0, 0.0));//verde
  G4VisAttributes* cyancolor = new G4VisAttributes(G4Colour(0.0, 1.0, 1.0));//cyan
  G4VisAttributes* magentacolor = new G4VisAttributes(G4Colour(1.0, 0.0, 1.0));//magenta
  G4VisAttributes* grigiocolor = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));//grigio
  G4VisAttributes* giallocolor = new G4VisAttributes(G4Colour(1.0, 1.0, 0.0));// giallo

  logicSpa->SetVisAttributes(verdecolor);
  logicAlSpa->SetVisAttributes(grigiocolor);
  logicH2OSpa->SetVisAttributes(blucolor);
  logicPorH2OSpa->SetVisAttributes(magentacolor);
  logicGH2OSpa->SetVisAttributes(grigiocolor);
  logicG2H2OSpa->SetVisAttributes(grigiocolor);
  logicG3H2OSpa->SetVisAttributes(grigiocolor);
  logicG4H2OSpa->SetVisAttributes(grigiocolor);
  logicG5H2OSpa->SetVisAttributes(grigiocolor);
  logicG6H2OSpa->SetVisAttributes(grigiocolor);
  logicG7H2OSpa->SetVisAttributes(grigiocolor);
  logicG8H2OSpa->SetVisAttributes(grigiocolor);
  logicG9H2OSpa->SetVisAttributes(grigiocolor);
  logicPbSpa->SetVisAttributes(verdecolor);
  logicFrontAlSpa->SetVisAttributes(grigiocolor);
  logicFront2AlSpa->SetVisAttributes(grigiocolor);
  logicFront3H2OSpa->SetVisAttributes(blucolor);
  logicFront4H2OSpa->SetVisAttributes(blucolor);
  logicEnd2H2OSpa->SetVisAttributes(blucolor);
  logicEnd4H2OBSpa->SetVisAttributes(cyancolor);
  logicFront5AlSpa->SetVisAttributes(grigiocolor);
  logicFront6AlSpa->SetVisAttributes(grigiocolor);
  logicEnd1AlSpa->SetVisAttributes(grigiocolor);
  logicEnd3AlSpa->SetVisAttributes(grigiocolor);
  //  logicGrid1->SetVisAttributes(grigiocolor);
 
  logicGr1->SetVisAttributes(grigiocolor);
  logicGr2->SetVisAttributes(grigiocolor);
  logicGr3->SetVisAttributes(grigiocolor);
  logicGr4->SetVisAttributes(grigiocolor);
  logicGr5->SetVisAttributes(grigiocolor);
  //
  logicGr6->SetVisAttributes(grigiocolor);
  logicGr7->SetVisAttributes(grigiocolor);
  //  logicGr8->SetVisAttributes(grigiocolor);
  logicGr9->SetVisAttributes(grigiocolor);
  //  logicGr10->SetVisAttributes(grigiocolor);
  logicGr11->SetVisAttributes(grigiocolor);
  //  logicGr11bis->SetVisAttributes(grigiocolor);
  //  logicGr12->SetVisAttributes(grigiocolor);
  logicGr13->SetVisAttributes(grigiocolor);
  //  logicGr14->SetVisAttributes(grigiocolor);
  logicGr15->SetVisAttributes(grigiocolor);
  // logicGr15bis->SetVisAttributes(grigiocolor);
  //
  logicGr16->SetVisAttributes(grigiocolor);
  logicGr17->SetVisAttributes(grigiocolor);
  // logicGr18->SetVisAttributes(grigiocolor);
  logicGr19->SetVisAttributes(grigiocolor);
  // logicGr20->SetVisAttributes(grigiocolor);
  logicGr21->SetVisAttributes(grigiocolor);
  // logicGr22->SetVisAttributes(grigiocolor);
  logicGr23->SetVisAttributes(grigiocolor);
  // logicGr24->SetVisAttributes(grigiocolor);
  logicGr25->SetVisAttributes(grigiocolor);
  //
  logicGr26->SetVisAttributes(grigiocolor);
  logicGr27->SetVisAttributes(grigiocolor);
  //  logicGr28->SetVisAttributes(grigiocolor);
  logicGr29->SetVisAttributes(grigiocolor);
  // logicGr30->SetVisAttributes(grigiocolor);
  logicGr31->SetVisAttributes(grigiocolor);
  // logicGr32->SetVisAttributes(grigiocolor);
  logicGr33->SetVisAttributes(grigiocolor);
  // logicGr34->SetVisAttributes(grigiocolor);
  logicGr35->SetVisAttributes(grigiocolor);
  //
  logicGr36->SetVisAttributes(grigiocolor);
  logicGr37->SetVisAttributes(grigiocolor);
  // logicGr38->SetVisAttributes(grigiocolor);
  logicGr39->SetVisAttributes(grigiocolor);
  // logicGr40->SetVisAttributes(grigiocolor);
  //  logicGr41->SetVisAttributes(grigiocolor);
  logicGr42->SetVisAttributes(grigiocolor);
  // logicGr43->SetVisAttributes(grigiocolor);
  //
  //
  logicGr44->SetVisAttributes(grigiocolor);
  logicGr45->SetVisAttributes(grigiocolor);
  // logicGr46->SetVisAttributes(grigiocolor);
  logicGr47->SetVisAttributes(grigiocolor);
  // logicGr48->SetVisAttributes(grigiocolor);
  // logicGr49->SetVisAttributes(grigiocolor);
  logicGr50->SetVisAttributes(grigiocolor);
  // logicGr51->SetVisAttributes(grigiocolor);
  //
 
  logicAirEnd->SetVisAttributes(verdecolor);
  logicGrigliaEnd->SetVisAttributes(grigiocolor);
  logicGriglia2End->SetVisAttributes(grigiocolor);
  logicDummy2->SetVisAttributes(redcolor);
  logicDummy2Vuoto->SetVisAttributes(giallocolor);
  logicPixelGrid2->SetVisAttributes(giallocolor);
  logicDum2UP->SetVisAttributes(redcolor);
    /*
  G4VisAttributes* Targetcolor = new G4VisAttributes(G4Colour(1.0, 0.0, 0.0));//red
  G4VisAttributes* Target1color = new G4VisAttributes(G4Colour(0.0, 0.0, 1.0));//blu
  G4VisAttributes* Target2color = new G4VisAttributes(G4Colour(0.0, 1.0, 0.0));//verde
  G4VisAttributes* Target3color = new G4VisAttributes(G4Colour(0.0, 1.0, 1.0));//cyan
  G4VisAttributes* Target4color = new G4VisAttributes(G4Colour(1.0, 0.0, 1.0));//magenta
  G4VisAttributes* Housingcolor = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));//grigio
  // G4VisAttributes* TappoCsIcolor = new G4VisAttributes(G4Colour(1.0, 1.0, 0.0));
  logicTarget->SetVisAttributes(Targetcolor);//red  
  logicTarget1->SetVisAttributes(Target1color);//blu  
  logicTarget2->SetVisAttributes(Target2color);//green  
  logicTarget3->SetVisAttributes(Target3color);//cyan  
  logicTarget4->SetVisAttributes(Target4color);//magenta  
  //logicTappoCsI->SetVisAttributes(TappoCsIcolor);//giallo
  logicHousing->SetVisAttributes(Housingcolor);//grigio    
  logicTop->SetVisAttributes(Housingcolor);//grigio    
  logicPMT->SetVisAttributes(Housingcolor);//grigio    
  G4cout << *(G4Material::GetMaterialTable()) << G4endl;

     */
 
  /*  
  G4VisAttributes* Spacolor = new G4VisAttributes(G4Colour(1.0, 0.0, 0.0));
  logicSpa->SetVisAttributes(Spacolor); // red
  logicDum2->SetVisAttributes(Spacolor); // red
  logicDum2UP->SetVisAttributes(Spacolor); // red
  G4VisAttributes* Modcolor = new G4VisAttributes(G4Colour(0.0, 0.0, 1.0));
  logicMod->SetVisAttributes(Modcolor); // blue
  logicMod2->SetVisAttributes(Modcolor); // blue
  logicHSpa->SetVisAttributes(Modcolor); // blue
  logicFAl2->SetVisAttributes(Modcolor); // blue
  G4VisAttributes* C1Icolor = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
  logicH->SetVisAttributes(C1Icolor); // gray
  logicFAl->SetVisAttributes(C1Icolor); // gray
  logicH2->SetVisAttributes(C1Icolor); // gray
  logicHoSpa->SetVisAttributes(C1Icolor); // gray
  */

  G4cout << *(G4Material::GetMaterialTable()) << G4endl;
  return physiWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::PrintParameters()
{
  //  G4cout << "\n The Box is " << G4BestUnit(fBoxSize,"Length")
  //       << " of " << fMaterial->GetName() 
  //       << "\n \n" << fMaterial << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::SetMaterial(G4String materialChoice)
{
  // search the material by its name
  ////G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);
  G4Material* pttoMaterial = 
     G4NistManager::Instance()->FindOrBuildMaterial(materialChoice);
  
  if (pttoMaterial) { fMaterial = pttoMaterial;
    } else {
    G4cout << "\n--> warning from DetectorConstruction::SetMaterial : "
           << materialChoice << " not found" << G4endl;
  }              
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::SetSize(G4double value)
{
  fBoxSize = value;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"

void DetectorConstruction::SetMagField(G4double fieldValue)
{
  //apply a global uniform magnetic field along Z axis
  G4FieldManager* fieldMgr
   = G4TransportationManager::GetTransportationManager()->GetFieldManager();

  if (fMagField) delete fMagField;        //delete the existing magn field

  if (fieldValue!=0.)                        // create a new one if non nul
    {
      fMagField = new G4UniformMagField(G4ThreeVector(0.,0.,fieldValue));
      fieldMgr->SetDetectorField(fMagField);
      fieldMgr->CreateChordFinder(fMagField);
    }
   else
    {
      fMagField = 0;
      fieldMgr->SetDetectorField(fMagField);
    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4RunManager.hh"

void DetectorConstruction::UpdateGeometry()
{
  G4RunManager::GetRunManager()->DefineWorldVolume(ConstructVolumes());
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
