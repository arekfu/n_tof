//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//
#include "G4ios.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"
#include "G4Event.hh"
#include "SteppingAction.hh"
#include "G4Track.hh"
#include "globals.hh"
#include "G4SteppingManager.hh"
#include "DetectorConstruction.hh"
#include "EventAction.hh"
#include "G4Step.hh"

#include "PrimaryGeneratorAction.hh"
#include "EventAction.hh"
#include "HistoManager.hh"
#include "G4ParticleTypes.hh"
#include "G4HadronicProcess.hh"


// servono per identificare i processi ottici di bordo
#include "G4EventManager.hh"
#include "G4ProcessManager.hh"
#include "G4StepPoint.hh"
#include "G4TrackStatus.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4OpBoundaryProcess.hh"
#include "G4SystemOfUnits.hh"


#include "SteppingActionMessenger.hh"
#include "G4UImanager.hh"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <math.h>

#include "UserTrackInformation.hh"

using namespace std;

//FILE *t1;
//FILE *t2;
//FILE *t1bis;
//FILE *t2bis;
////FILE *t4;// commentare finita la prova
//FILE *t5;


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

SteppingAction::SteppingAction(EventAction *ea, const G4bool withColl) :
  eventAction(ea),
  withCollisionNtuple(withColl)
{
  stepMessenger = new SteppingActionMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

SteppingAction::~SteppingAction()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void SteppingAction::UserSteppingAction(const G4Step* fStep)
{

  G4int RID= G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  G4int PID = fStep->GetTrack()->GetParentID();
  G4int TID = fStep->GetTrack()->GetTrackID();

  G4ThreeVector position;
  G4double xp;
  G4double yp;
  G4double zp;
  position = fStep->GetTrack()->GetPosition();
  xp = position.x() / cm;
  yp = position.y() / cm;
  zp = position.z() / cm;
  G4ThreeVector momo = fStep->GetTrack()->GetDynamicParticle()->GetMomentumDirection();
  G4double xm;
  G4double ym;
  G4double zm;
  xm = momo.x();
  ym = momo.y();
  zm = momo.z();
  G4double globalTime = fStep->GetTrack()->GetGlobalTime() / s;
  G4int nColls = static_cast<UserTrackInformation*>(fStep->GetTrack()->GetUserInformation())->GetNColls();


  G4Track* fTrack = fStep->GetTrack();
  G4int StepNo = fTrack->GetCurrentStepNumber();
  if(StepNo >= 100000) fTrack->SetTrackStatus(fStopAndKill);//era 10000


// parte per lo studio del flusso

  G4ParticleDefinition* par = fStep->GetTrack()->GetDefinition();
  G4String particleName = par->GetParticleName();
  G4double energyn = fStep->GetTrack()->GetDynamicParticle()->GetKineticEnergy() / MeV;
  G4AnalysisManager* analysis = G4AnalysisManager::Instance();

  /*
  // prove 30/03/2015 per controllare se la macro gps geneare un fascio gaussiano nelle coordinate
  // da commentare finita la prova
  if ((PID == 0) && (TID == 1) && (particleName  == "proton") && (fStep->GetTrack()->GetCurrentStepNumber()==1)) {
    fprintf(t4,"%E %s %E %s %E\n",xp," ",yp," ",zp);
    fflush(t4);
  }
  // fine prove 30/03/2015 per controllare se la macro gps geneare un fascio gaussiano nelle coordinate
  */

  /*
 if (fStep->GetPreStepPoint()->GetPhysicalVolume()->GetName() == "physiSource")
    {
      if (fStep->GetPostStepPoint()->GetPhysicalVolume()->GetName() == "physiFAl")
      {
        if (particleName  == "proton")
          {
            G4double globalTime1g = fStep->GetTrack()->GetGlobalTime();
            //          G4cout<<"PROTON: "<< fStep->GetTrack()->GetGlobalTime()<<G4endl;
      fprintf(t2,"%d %s %E %s %E %s %E %s %E %s %E %s %E %s %E %s %E\n",RID," ",energyn," ",xp," ",yp, " ",zp," ",xm," ",ym," ",zm," ",globalTime1g);
      fflush(t2);
          }
      }
    }

  */

  if (particleName  == "pi0")
    {
      if (fStep->GetTrack()->GetCurrentStepNumber()==1)
      {
        /*
           G4cout<<"PI0!!!!!!!!!!! "<<G4endl;
           G4cout<<"RID ="<<RID<<G4endl;
           G4cout<<"PID ="<<PID<<G4endl;
           G4cout<<"TID ="<<TID<<G4endl;
           G4cout<<"Particle Name ="<<particleName<<G4endl;
           G4cout<<"pdg ="<<codice<<G4endl;
           G4cout<<"x ="<<xp<<G4endl;
           G4cout<<"y ="<<yp<<G4endl;
           G4cout<<"z ="<<zp<<G4endl;
           G4cout<<"energy ="<<energyn<<G4endl;
           */
        //    fprintf(t5,"%d %s %d %s %d %s %E %s %E %s %E %s %E\n",RID," ",PID," ",TID," ",energyn," ",xp," ",yp, " ",zp);
        //    fflush(t5);
        const G4int pi0ID = 4;
        analysis->FillNtupleIColumn(pi0ID, 0, RID);
        analysis->FillNtupleIColumn(pi0ID, 1, PID);
        analysis->FillNtupleIColumn(pi0ID, 2, TID);
        analysis->FillNtupleFColumn(pi0ID, 3, xp);
        analysis->FillNtupleFColumn(pi0ID, 4, yp);
        analysis->FillNtupleFColumn(pi0ID, 5, zp);
        analysis->FillNtupleFColumn(pi0ID, 6, energyn);
        analysis->AddNtupleRow(pi0ID);
      }
    }

  if (fStep->GetPreStepPoint()->GetPhysicalVolume()->GetName() == "physiDum2")
    {
      if (fStep->GetPostStepPoint()->GetPhysicalVolume()->GetName() == "physiWorld")
      // neutroni che escono in avanti
      {
        if (particleName  == "neutron")
          {
            if (energyn > 0.)
            {
              // if ((zm>=0.9994) && (zm<=1.0))
              if ((zm>=0.98480775) && (zm<=1.0))
              {
                //              G4cout<<" 1 "<<RID<<" "<<PID<<" "<<TID<<" "<<globalTime1<<" "<<energyn<<G4endl;         
                // fprintf(t1,"%d %s %d %s %d %s %f %s %f %s %s %s %f %s %f %s %f %s %f %s %f %s %f\n",RID," ",PID," ",TID," ",energyn," ",globalTime1," ","n"," ",xp," ",yp, " ",zp," ",xm," ",ym," ",zm);
                //fprintf(t1,"%d %s %E %s %E %s %E %s %E %s %E %s %E %s %E %s %E\n",RID," ",energyn," ",xp," ",yp, " ",zp," ",xm," ",ym," ",zm," ",globalTime1);
                //fflush(t1);
                const G4int nflux1ID = 0;
                analysis->FillNtupleIColumn(nflux1ID, 0,  RID);
                analysis->FillNtupleIColumn(nflux1ID, 1,  PID);
                analysis->FillNtupleIColumn(nflux1ID, 2,  TID);
                analysis->FillNtupleIColumn(nflux1ID, 3,  nColls);
                analysis->FillNtupleFColumn(nflux1ID, 4,  xp);
                analysis->FillNtupleFColumn(nflux1ID, 5,  yp);
                analysis->FillNtupleFColumn(nflux1ID, 6,  zp);
                analysis->FillNtupleFColumn(nflux1ID, 7,  xm);
                analysis->FillNtupleFColumn(nflux1ID, 8,  ym);
                analysis->FillNtupleFColumn(nflux1ID, 9,  zm);
                analysis->FillNtupleFColumn(nflux1ID, 10,  energyn);
                analysis->FillNtupleFColumn(nflux1ID, 11, globalTime);
                analysis->AddNtupleRow(nflux1ID);
              }
            }
          }// fine neutroni
      
        if (particleName  == "gamma")
          {
            if (energyn > 0.)
            {
              // if ((zm>=0.9994) && (zm<=1.0))
              if ((zm>=0.98480775) && (zm<=1.0))
              {
                //              G4cout<<" 1 "<<RID<<" "<<PID<<" "<<TID<<" "<<globalTime1<<" "<<energyn<<G4endl;         
                //fprintf(t1bis,"%d %s %d %s %d %s %E %s %E %s %E %s %E %s %E %s %E %s %E %s %E\n",RID," ",PID," ",TID," ",energyn," ",globalTime1g," ",xp," ",yp, " ",zp," ",xm," ",ym," ",zm);
                //fflush(t1bis);
                const G4int gflux1ID = 1;
                analysis->FillNtupleIColumn(gflux1ID, 0,  RID);
                analysis->FillNtupleIColumn(gflux1ID, 1,  PID);
                analysis->FillNtupleIColumn(gflux1ID, 2,  TID);
                analysis->FillNtupleIColumn(gflux1ID, 3,  nColls);
                analysis->FillNtupleFColumn(gflux1ID, 4,  xp);
                analysis->FillNtupleFColumn(gflux1ID, 5,  yp);
                analysis->FillNtupleFColumn(gflux1ID, 6,  zp);
                analysis->FillNtupleFColumn(gflux1ID, 7,  xm);
                analysis->FillNtupleFColumn(gflux1ID, 8,  ym);
                analysis->FillNtupleFColumn(gflux1ID, 9,  zm);
                analysis->FillNtupleFColumn(gflux1ID, 10,  energyn);
                analysis->FillNtupleFColumn(gflux1ID, 11, globalTime);
                analysis->AddNtupleRow(gflux1ID);
              }
              }
          }//fine gamma
      
      }
    }

  if (fStep->GetPreStepPoint()->GetPhysicalVolume()->GetName() == "physiDum2UP")
    {
      if (fStep->GetPostStepPoint()->GetPhysicalVolume()->GetName() == "physiWorld")
        // neutroni che escono sopra
        {
          if (particleName  == "neutron")
            {
              if (energyn > 0.)
                {
              // if ((ym>=0.9994)&&(ym<=1.0))
              if ((ym>=0.98480775) && (ym<=1.0))
              {
                //fprintf(t2,"%d %s %E %s %E %s %E %s %E %s %E %s %E %s %E %s %E\n",RID," ",energyn," ",xp," ",yp, " ",zp," ",xm," ",ym," ",zm," ",globalTime2);
                //fflush(t2);
                const G4int nflux2ID = 2;
                analysis->FillNtupleIColumn(nflux2ID, 0,  RID);
                analysis->FillNtupleIColumn(nflux2ID, 1,  PID);
                analysis->FillNtupleIColumn(nflux2ID, 2,  TID);
                analysis->FillNtupleIColumn(nflux2ID, 3,  nColls);
                analysis->FillNtupleFColumn(nflux2ID, 4,  xp);
                analysis->FillNtupleFColumn(nflux2ID, 5,  yp);
                analysis->FillNtupleFColumn(nflux2ID, 6,  zp);
                analysis->FillNtupleFColumn(nflux2ID, 7,  xm);
                analysis->FillNtupleFColumn(nflux2ID, 8,  ym);
                analysis->FillNtupleFColumn(nflux2ID, 9,  zm);
                analysis->FillNtupleFColumn(nflux2ID, 10,  energyn);
                analysis->FillNtupleFColumn(nflux2ID, 11, globalTime);
                analysis->AddNtupleRow(nflux2ID);
              }
                }
            }// fine neutroni

      
        if (particleName  == "gamma")
          {
            if (energyn > 0.)
            {
 // if ((ym>=0.9994)&&(ym<=1.0))
              if ((ym>=0.98480775) && (ym<=1.0))
              {
                //              G4cout<<" 1 "<<RID<<" "<<PID<<" "<<TID<<" "<<globalTime1<<" "<<energyn<<G4endl;         
                //fprintf(t2bis,"%d %s %d %s %d %s %E %s %E %s %E %s %E %s %E %s %E %s %E %s %E\n",RID," ",PID," ",TID," ",energyn," ",globalTime2g," ",xp," ",yp, " ",zp," ",xm," ",ym," ",zm);
                //fflush(t2bis);
                const G4int gflux2ID = 3;
                analysis->FillNtupleIColumn(gflux2ID, 0,  RID);
                analysis->FillNtupleIColumn(gflux2ID, 1,  PID);
                analysis->FillNtupleIColumn(gflux2ID, 2,  TID);
                analysis->FillNtupleIColumn(gflux2ID, 3,  nColls);
                analysis->FillNtupleFColumn(gflux2ID, 4,  xp);
                analysis->FillNtupleFColumn(gflux2ID, 5,  yp);
                analysis->FillNtupleFColumn(gflux2ID, 6,  zp);
                analysis->FillNtupleFColumn(gflux2ID, 7,  xm);
                analysis->FillNtupleFColumn(gflux2ID, 8,  ym);
                analysis->FillNtupleFColumn(gflux2ID, 9,  zm);
                analysis->FillNtupleFColumn(gflux2ID, 10,  energyn);
                analysis->FillNtupleFColumn(gflux2ID, 11, globalTime);
                analysis->AddNtupleRow(gflux2ID);
              }
              }
          }//fine gamma
      
        }
    }

  // fine parte per lo studio del flusso


  G4int ih;


  // SFERA prima di OUTWORLD dove vengono killate le particelle, per
  // evitare il segmentation fault determinato dalla presenza del PostStep
  // nelle righe di codice precedenti


  if (fStep->GetPreStepPoint()->GetPhysicalVolume()->GetName() == "physiWorld")
    {
      // G4cout<<" sono nel physi World per andare verso il killing "<<G4endl;
      if (fStep->GetPostStepPoint()->GetPhysicalVolume()->GetName() == "physiKilling")
      {
        //  G4cout<<" sono nel  killing "<<G4endl;
        fStep->GetTrack()->SetTrackStatus(fStopAndKill);
      }
    }



  //initialisation of the nuclear channel identification
  //
  const G4StepPoint* endPoint = fStep->GetPostStepPoint();
  //  const G4VProcess* process   = endPoint->GetProcessDefinedStep();

  G4ParticleDefinition* particle = fStep->GetTrack()->GetDefinition();
  G4String partName = particle->GetParticleName();
  G4String nuclearChannel = partName;
  //G4HadronicProcess* hproc = (G4HadronicProcess*) process;
  // const G4Isotope* target = hproc->GetTargetIsotope();

  // check that an real interaction occured (eg. not a transportation)
  G4StepStatus stepStatus = endPoint->GetStepStatus();
  G4bool transmit = (stepStatus==fGeomBoundary || stepStatus==fWorldBoundary);
  if (transmit) return;

  // fill the collisions ntuple, if requested
  const G4TrackVector* secondary = fpSteppingManager->GetSecondary();
  if(endPoint->GetPhysicalVolume()->GetName()=="physiSpa") {
    const G4VProcess *process = endPoint->GetProcessDefinedStep();
    if(process && process->GetProcessType()==fHadronic) {
      const G4HadronicProcessType subtype = (G4HadronicProcessType) process->GetProcessSubType();
      UserTrackInformation *info = static_cast<UserTrackInformation*>(fStep->GetTrack()->GetUserInformation());
      if(subtype==fHadronInelastic || subtype==fCapture || subtype==fFission)
        info->Increment();
      if(withCollisionNtuple) {
        const G4int ntupleID = 5;
        const G4int newNColls = info->GetNColls();
        // fill the reaction ntuple
        const G4int pdg = particle->GetPDGEncoding();
        const G4int nSecondaries = secondary->size();
        const G4double reacEnergy =
          (fStep->GetPreStepPoint()->GetKineticEnergy()
           - fStep->GetTotalEnergyDeposit()) / MeV;
        G4HadronicProcess *hProcess = const_cast<G4HadronicProcess*>(dynamic_cast<const G4HadronicProcess*>(process));
        G4int targetA = 0;
        G4int targetZ = 0;
        if(hProcess) {
          const G4Isotope *target = hProcess->GetTargetIsotope();
          if(target) {
            targetA = target->GetA();
            targetZ = target->GetZ();
          }
        }
        analysis->FillNtupleIColumn(ntupleID, 0, RID);
        analysis->FillNtupleIColumn(ntupleID, 1, PID);
        analysis->FillNtupleIColumn(ntupleID, 2, TID);
        analysis->FillNtupleIColumn(ntupleID, 3, pdg);
        analysis->FillNtupleIColumn(ntupleID, 4, nSecondaries);
        analysis->FillNtupleIColumn(ntupleID, 5, newNColls);
        analysis->FillNtupleIColumn(ntupleID, 6, (G4int) subtype);
        analysis->FillNtupleIColumn(ntupleID, 7, targetA);
        analysis->FillNtupleIColumn(ntupleID, 8, targetZ);
        analysis->FillNtupleFColumn(ntupleID, 9, xp);
        analysis->FillNtupleFColumn(ntupleID, 10, yp);
        analysis->FillNtupleFColumn(ntupleID, 11, zp);
        analysis->FillNtupleFColumn(ntupleID, 12, reacEnergy);
        analysis->FillNtupleFColumn(ntupleID, 13, globalTime);
        analysis->AddNtupleRow(ntupleID);
      }
    }
  }

  //scattered primary particle (if any)
  //
  ih = 1;
  if (fStep->GetTrack()->GetTrackStatus() == fAlive) {
    G4double energy = endPoint->GetKineticEnergy();
    analysis->FillH1(ih,energy);
  }

  //secondaries
  //
  for (size_t lp=0; lp<(*secondary).size(); lp++) {
    ih=-1;
    particle = (*secondary)[lp]->GetDefinition();
    G4String name   = particle->GetParticleName();
    G4String type   = particle->GetParticleType();
    G4double charge = particle->GetPDGCharge();
    G4double energy = (*secondary)[lp]->GetKineticEnergy();

    //energy spectrum
    if (charge > 3.)  ih = 2;
    else if (particle == G4Gamma::Gamma() && energy>10*MeV)       ih = 3;
    else if (particle == G4Neutron::Neutron())   ih = 4;
    else if (particle == G4Proton::Proton())     ih = 5;
    else if (particle == G4PionZero::PionZero()) ih = 6;
    else if (particle == G4PionPlus::PionPlus())       ih = 7;
    else if (particle == G4PionMinus::PionMinus())       ih = 8;
    if(ih>0)
      analysis->FillH1(ih,energy);

    if(particle==G4PionZero::PionZero()) {
      ++eventAction->nPi0;
    } else if(particle==G4PionPlus::PionPlus() || particle==G4PionMinus::PionMinus()) {
      ++eventAction->nPiCharged;
    } else if(particle==G4Gamma::Gamma() && energy>10.*MeV) {
      ++eventAction->nGamma;
    } else if(particle==G4Neutron::Neutron()) {
      ++eventAction->nNeutron;
    }

  }

  // kill event after first interaction

  //  G4RunManager::GetRunManager()->AbortEvent();


}
