#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "Randomize.hh"
#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "ActionInitialization.hh"
#include "G4HadronicProcessStore.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"
#include <stdio.h>
#include <string.h>
#include <time.h>

// per usare un verbose diverso dal default
#include "SteppingVerbose.hh"

//test di Davide (luglio 2015) per i pi0
#include "G4INCLXXInterfaceStore.hh"
#include "G4INCLConfig.hh"
#include "G4INCLConfigEnums.hh"

#ifdef HAS_BOOST_PROGRAM_OPTIONS
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/variables_map.hpp>
namespace po = boost::program_options;
#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main(int argc,char** argv) {
  
  G4int numberOfThreads = 1;
  std::string crossSections = "multipion";
  std::string outputFileName = "nTOFFlux";
  G4bool withCollisionNtuple = false;
  G4long seed = -1;

#ifdef HAS_BOOST_PROGRAM_OPTIONS
  std::string macroFile;

  // define the CLI options
  po::options_description runOptDesc("Run options");
  runOptDesc.add_options()
    ("help,h", "produce this help message")
    ("threads,t", po::value<G4int>(&numberOfThreads)->default_value(1), "number of threads for the calculation")
    ("cross-sections", po::value<std::string>(&crossSections)->default_value("multipion"), "type of cross sections to use")
    ("with-collision-ntuple", po::value<G4bool>(&withCollisionNtuple)->default_value(false)->implicit_value(true), "whether we should produce an output collision ntuple")
    ("output,o", po::value<std::string>(&outputFileName)->default_value("nTOFFlux"), "stem of the output file")
    ("seed,s", po::value<G4long>(&seed)->default_value(-1), "seed for the PRNG")
    ;
  po::options_description hiddenOptDesc("Hidden options");
  hiddenOptDesc.add_options()
    ("input-file", po::value<std::string>(&macroFile), "macro file")
    ;

  po::options_description allOptions;
  allOptions.add(runOptDesc).add(hiddenOptDesc);

  po::positional_options_description p;
  p.add("input-file", 1);

  // Disable guessing of option names
  const int cmdstyle =
    po::command_line_style::default_style &
    ~po::command_line_style::allow_guessing;

  // Result of the option processing
  po::variables_map variablesMap;
  po::store(po::command_line_parser(argc, argv).
            style(cmdstyle).
            options(allOptions).positional(p).run(), variablesMap);
  po::notify(variablesMap);

  // -h/--help option
  if(variablesMap.count("help")) {
    G4cout
      << "Usage: nTOFFlux [options] <macro_file>" << G4endl
      << runOptDesc << G4endl;
    return 0;
  }

  // Detect interactive mode (if no arguments) and define UI session
  G4UIExecutive* ui = 0;
  if(macroFile.empty()) {
    ui = new G4UIExecutive(argc, argv);
  }
#else
  // Detect interactive mode (if no arguments) and define UI session
  G4UIExecutive* ui = 0;
  if ( argc == 1 ) {
    ui = new G4UIExecutive(argc, argv);
  }
#endif

  //// creazione del seed legato al tempo
  if(seed<0) {
    time_t seconds = time(NULL);
    seed = (G4long) seconds;
    G4cout << "Seed determined from the system time: " << seed << G4endl;
  }

  // random engine
  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
  CLHEP::HepRandom::setTheSeed(seed);

 //my Verbose output class (solo in caso di bisogno)
  G4VSteppingVerbose::SetInstance(new SteppingVerbose);
  
  //Construct the default run manager
#ifdef G4MULTITHREADED
  G4MTRunManager* runManager = new G4MTRunManager;
#ifdef HAS_BOOST_PROGRAM_OPTIONS
  // -t/--threads option
  if(variablesMap.count("threads")) {
    runManager->SetNumberOfThreads(numberOfThreads);
  }
#endif

#else
  G4RunManager* runManager = new G4RunManager;
#ifdef HAS_BOOST_PROGRAM_OPTIONS
  // -t/--threads option
  if(variablesMap.count("threads")) {
    G4cout << "WARNING: -t/--threads option is ignored in sequential mode" << G4endl;
  }
#endif
#endif

  //set mandatory initialization classes
  runManager->SetUserInitialization(new DetectorConstruction());
  runManager->SetUserInitialization(new PhysicsList);
//  runManager->SetUserAction(new PrimaryGeneratorAction());

  //set user action classes
  runManager->SetUserInitialization(new ActionInitialization(crossSections, outputFileName, withCollisionNtuple));

  G4HadronicProcessStore::Instance()->SetVerbose(2);

  // Initialize visualization
  //
  G4VisManager* visManager = new G4VisExecutive;
  // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
  // G4VisManager* visManager = new G4VisExecutive("Quiet");
  visManager->Initialize();

  // Get the pointer to the User Interface manager
  G4UImanager* UImanager = G4UImanager::GetUIpointer();

  // Process macro or start UI session
  //
  if ( ! ui ) {
    // batch mode
    G4String command = "/control/execute ";
    UImanager->ApplyCommand(command+macroFile);
  }
  else {
    // interactive mode
    UImanager->ApplyCommand("/control/execute init_vis.mac");
    ui->SessionStart();
    delete ui;
  }

  // Job termination
  // Free the store: user actions, physics_list and detector_description are
  // owned and deleted by the run manager, so they should not be deleted
  // in the main() program !

  delete visManager;
  delete runManager;
  return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
