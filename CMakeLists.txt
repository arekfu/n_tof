#----------------------------------------------------------------------------
# Setup the project
cmake_minimum_required(VERSION 2.6 FATAL_ERROR)
project(nTOFFlux)

#----------------------------------------------------------------------------
# Find Geant4 package, activating all available UI and Vis drivers by default
# You can set WITH_GEANT4_UIVIS to OFF via the command line or ccmake/cmake-gui
# to build a batch mode only executable
#
option(WITH_GEANT4_UIVIS "Build example with Geant4 UI and Vis drivers" ON)
if(WITH_GEANT4_UIVIS)
  find_package(Geant4 REQUIRED ui_all vis_all)
else()
  find_package(Geant4 REQUIRED)
endif()

#----------------------------------------------------------------------------
# Setup Geant4 include directories and compile definitions
#
include(${Geant4_USE_FILE})

#----------------------------------------------------------------------------
# Locate sources and headers for this project
#
include_directories(${PROJECT_SOURCE_DIR}/include 
                    ${Geant4_INCLUDE_DIR})
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cc)
file(GLOB headers ${PROJECT_SOURCE_DIR}/include/*.hh)

# find boost::program_options
find_package(Boost 1.35.0 COMPONENTS program_options)
if(Boost_PROGRAM_OPTIONS_FOUND)
  add_definitions(-DHAS_BOOST_PROGRAM_OPTIONS)
endif(Boost_PROGRAM_OPTIONS_FOUND)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS})

#----------------------------------------------------------------------------
# Add the executable, and link it to the Geant4 libraries
#
add_executable(nTOFFlux nTOFFlux.cc ${sources} ${headers})
target_link_libraries(nTOFFlux ${Boost_LIBRARIES} ${Geant4_LIBRARIES} )

#----------------------------------------------------------------------------
# Copy all scripts to the build directory, i.e. the directory in which we
# build nTOFFlux. This is so that we can run the executable directly because it
# relies on these scripts being in the current working directory.
#
set(nTOFFlux_SCRIPTS
  ntofflux.in vis.mac init_vis.mac gun.mac
  )

foreach(_script ${nTOFFlux_SCRIPTS})
  configure_file(
    ${PROJECT_SOURCE_DIR}/${_script}
    ${PROJECT_BINARY_DIR}/${_script}
    COPYONLY
    )
endforeach()

#----------------------------------------------------------------------------
# Install the executable to 'bin' directory under CMAKE_INSTALL_PREFIX
#
install(TARGETS nTOFFlux DESTINATION bin)


# g42so
find_program(G42SO g42so PATHS ~/src/t4g4interface/g42so)
set(LIBNTOF_FILES
  "${CMAKE_SOURCE_DIR}/src/DetectorConstruction.cc"
  "${CMAKE_SOURCE_DIR}/src/DetectorMessenger.cc"
  "${CMAKE_SOURCE_DIR}/src/PrimaryGeneratorAction.cc"
  "${CMAKE_SOURCE_DIR}/src/ChamberParameterisation.cc"
  )
if(G42SO)
  message(STATUS "Found g42so: ${G42SO}")
  add_custom_command(OUTPUT libntof.so
    COMMAND "${G42SO}" -o libntof.so -I "${CMAKE_SOURCE_DIR}/include" --geant4-config "${Geant4_DIR}/../../bin/geant4-config" ${LIBNTOF_FILES}
    DEPENDS ${LIBNTOF_FILES}
    VERBATIM)
  add_custom_target(libntof DEPENDS libntof.so)
else()
  message(STATUS "Could not find g42so")
endif()
