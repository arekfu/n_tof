#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <limits>
#include <ios>
#include <math.h>
#include <vector>

using namespace std;

int main(int argc, char* argv[])
{
  char nome[10];
  string unione[1000];
  
  for(int i=0; i<1000; i++)
    {
      string saluto= "ntofflux_";
      sprintf(nome,"%d", i);
      unione[i] = saluto + nome;
      cout<<unione[i]<<endl;
    }
  
  for(int i=0; i<1000; i++)
    {
      ofstream in((unione[i]).c_str()); 
      in<<"#"<<endl;
      in<<"/control/verbose 0"<<endl;
      in<<"/run/verbose 0"<<endl;
      in<<"/tracking/verbose 0"<<endl;
      in<<"/testhadr/CutsAll  0.006 mm"<<endl;
      //      in<<"/testhadr/Physics  FTFP_BERT_HP"<<endl;
      in<<"/testhadr/Physics  FTFP_INCLXX_HP"<<endl;
      in<<"/testhadr/Physics  emstandard_opt0"<<endl;
      in<<"/run/initialize"<<endl;
      in<<"/gps/source/intensity 1"<<endl;
      in<<"/gps/particle proton"<<endl;
      in<<"/gps/energy 20 GeV"<<endl;
      in<<"/gps/pos/type Beam"<<endl;
      //      in<<"/gps/pos/type Plane"<<endl;
      in<<"/gps/pos/shape Circle"<<endl;
      in<<"/gps/pos/centre 0.0 0.0 -21.300001"<<endl;
      // in<<"/gps/pos/radius 1.5"<<endl;//era 0.5
      in<<"/gps/pos/radius 0.1"<<endl;
      in<<"/gps/pos/sigma_r 1.5"<<endl;
      //      in<<"/gps/direction  0. 0. 1."<<endl;
      in<<"/gps/direction -0.17364818 0.0 0.98480775"<<endl;// era 0.0 -0.17364818 0.98480775
      in<<"/process/list"<<endl;
      //      in<<"/analysis/setFileName ntoffluxgrid3v10_"<<i<<endl;
      // in<<"/analysis/h1/set 13 240  0  240"<<endl; 
      // in<<"/analysis/h1/set 14 240  0  240"<<endl; 
      // in<<"/analysis/h1/set 15 100  0  1000"<<endl;
      in<<"/run/beamOn 1000"<<endl;//era 2000
      in<<"#"<<endl;      
      in.close();    
    }
  
  cout<<"THAT'S ALL FOLKS!!"<<endl;
  return 0;
}
